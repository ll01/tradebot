module bitbucket.org/ll01/tradebot

go 1.12

require (
	github.com/CoinAPI/coinapi-sdk v0.0.0-20190801134305-ac3c8beccf71
	github.com/adshao/go-binance v0.0.0-20190731124949-0ec96107e98d
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23
	github.com/dchest/uniuri v0.0.0-20160212164326-8902c56451e9
	github.com/gorilla/websocket v1.4.0
	github.com/gorjan-mishevski/puzzle v0.0.0-20180105202601-b01669801428
	github.com/json-iterator/go v1.1.7
	github.com/line/line-bot-sdk-go v6.3.0+incompatible
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/montanaflynn/stats v0.5.0
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24
	github.com/thebotguys/signalr v0.0.0-20190119054324-787ebe6729fc // indirect
	github.com/toorop/go-bittrex v0.0.0-20190315075857-b4a20c2db009
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	nanomsg.org/go/mangos/v2 v2.0.2
)
