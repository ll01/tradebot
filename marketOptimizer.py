import aiohttp
import urllib
import asyncio
import json
import subprocess
import shlex
import sqlite3
import os
import multiprocessing
import copy
import itertools
from multiprocessing import pool
from itertools import repeat
from operator import itemgetter


async def fetch(session, url):
	async with session.get(url) as response:
		return await response.text()


def generateSettingsCluseter(assets, mainJson):
	ogClustter = mainJson["marketcluster"][0]
	for asset in assets:
		newCluster = copy.deepcopy(ogClustter)
		newCluster["tradecoins"] = [asset]
		mainJson["marketcluster"].append(newCluster)
	return mainJson

def runSubprocess(fileName):
	tradeBotProcess = subprocess.Popen(shlex.split(
		"./tradebot {}".format(fileName)), stderr=subprocess.PIPE, stdout=subprocess.PIPE)
	tradeBotProcess.communicate()


def findCount(conn):
	c = conn.cursor()
	t = ('sell',)
	count = 0
	try:
		c.execute('SELECT count(*) FROM test WHERE test.type=?', t)
		count = c.fetchone()[0]
	except Exception as e:
		print(e)
	return count

def calculatProfitSingle(conn):
	pass

def calculateProfit(conn):
	c = conn.cursor()
	t = ('sell',)
	ratios = []
	try:
		c.execute(
			'SELECT max(test.amount) / min(test.amount), test.marketString FROM test WHERE test.type=? group by (test.marketString)', t)
		ratios = c.fetchall()
	except Exception as e:
		print(e)
	return ratios


def calculateGain(conn):
	c = conn.cursor()
	t = ('sell',)
	profitValue = 0
	try:
		c.execute('SELECT count(*) FROM test WHERE test.type=?', t)
		count = c.fetchone()[0]
	except Exception as e:
		print(e)

	return profitValue


def StochasticStrategyOpt():
	strats = []
	for undersold in range(30):
		for kperiod in range(5, 30):
			for dperiod in range(1, 5):
				for persistence in range(20):
					strat = {
						"name": "StochasticStrategy",
						"overbought": 0,
						"undersold": undersold,
						"kperiod": kperiod,
						"dperiod": dperiod,
						"persistence": persistence
					}
					strats.append(strat)
	return strats


def StanderdDeviationStrategyOpt():
	strats = []
	for undersold in range(1, 10, 1):
		for persistence in range(10):
			strat = {
				"name": "StanderdDeviationStrategy",
				"overbought": 0,
				"undersold": 10,
				"target": undersold /100,
				"persistence": persistence
			},
			strats.append(strat)
	return strats


def feches(symbol, base, fast):
	#   https://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments
	if symbol["quoteAsset"] == str.upper(base) and symbol["status"] == "TRADING":
		if fast is not True:
			priceData = urllib.request.urlopen(
				"https://api.binance.com/api/v3/avgPrice?symbol={}"
				.format(symbol["symbol"])).read()
			priceJson = json.loads(priceData)
			price = float(priceJson["price"])
			if price > 0.00000100:
				return symbol
		else:
			return symbol


async def main():
	settingsFileLocation = "./settings.json"
	base = "BTC"
	top = True
	fast = True
	coinSize = 5
	baseAssets = []
	topCoins = []
	async with aiohttp.ClientSession() as session:
		marketData = await fetch(session, "https://api.binance.com/api/v1/exchangeInfo")
		marketJson = json.loads(marketData)
		symbols = marketJson["symbols"]
		rankDataTask = fetch(
			session, "https://api.coinranking.com/v1/public/coins")
		rankData = await rankDataTask
		rankJson = json.loads(rankData)
		for symbol in rankJson["data"]["coins"]:
			topCoins.append(symbol["symbol"])
	with multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()) as pool:
		fetchResults = pool.starmap(feches, zip(symbols, repeat(base),repeat(fast)))
	for result in fetchResults:
		if result != None and result["baseAsset"] in topCoins:
			baseAssets.append(result["baseAsset"])

	marketResults = []
	p = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
	mainFile = open(settingsFileLocation, "r")
	mainFileJson = json.loads(mainFile.read())

	settingsJson = generateSettingsCluseter(baseAssets, mainFileJson)
	fileName = "optimize" + os.path.basename(settingsFileLocation)
	print(fileName)
	if os.path.exists(fileName):
		os.remove(fileName)
	optimizedFile = open(fileName, "w")
	optimizedFile.write(json.dumps(settingsJson))
	optimizedFile.close
	runSubprocess(fileName)
	databaseName = mainFileJson["databasename"] + ".sqlite"

	conn = sqlite3.connect(databaseName)

	data = calculateProfit(conn).sort(key=itemgetter(0),reverse=True)
	conn.close()
	if data != None and  len(data) > coinSize:
		data = data[:5]
	print(data)
	# strats1 = StochasticStrategyOpt()
	# strats2 = StanderdDeviationStrategyOpt()
	print("hi")



def makePermitations(data):
	permutations = []
	for item in range(1, len(data) + 1):
		for subset in itertools.permutations(data, item):
			permutations.append(subset)


if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	loop.run_until_complete(main())
