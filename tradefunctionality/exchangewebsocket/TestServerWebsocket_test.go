package exchangewebsocket

import (
	"reflect"
	"testing"
)

// func Test_rng_roll(t *testing.T) {
// 	type fields struct {
// 		chance int
// 	}
// 	tests := []struct {
// 		name   string
// 		fields fields
// 		want   bool
// 	}{
// 		// TODO: Add test cases.
// 		{"100% chance", fields{100}, true},
// 		{"0% chance", fields{0}, false},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			r := &rng{
// 				chance: tt.fields.chance,
// 			}
// 			if got := r.roll(); got != tt.want {
// 				t.Errorf("rng.roll() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func Test_decodeMessages(t *testing.T) {
	type args struct {
		filePath string
	}
	tests := []struct {
		name string
		args args
		want []replayMessage
	}{
		// TODO: Add test cases.
		{"basic", args{"/Users/mcphersd/Downloads/logs.bin"}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := decodeMessages(tt.args.filePath); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("decodeMessages() = %v, want %v", got, tt.want)
			}
		})
	}
}
