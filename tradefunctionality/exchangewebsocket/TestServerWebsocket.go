package exchangewebsocket

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/logs"

	"github.com/gorilla/websocket"
)

// TestServerWebsocket makes a server on the local host
type TestServerWebsocket struct {
	priceMessages [][]byte
	tradeMessages [][]byte
	messages      [][]byte
	counter       int
}
type httpFunc func(w http.ResponseWriter, r *http.Request)

// RunWebsocket creates the server and connects to a local client to be read from
func (t *TestServerWebsocket) RunWebsocket(subscriberFunc httpFunc) *websocket.Conn {
	connectionurl := "/connect"

	http.HandleFunc(connectionurl, subscriberFunc)
	baseurl := "127.0.0.1:5564"
	go func() {
		log.Fatal(http.ListenAndServe(baseurl, nil))
	}()
	url := url.URL{
		Scheme: "ws",
		Host:   baseurl,
		Path:   connectionurl,
	}
	conn, _, err := websocket.DefaultDialer.Dial(url.String(), nil)
	if err != nil {
		fmt.Println(err)
	}
	return conn
}

// Subscriber saves the client connection TO DO: move
func (t *TestServerWebsocket) SubscribeAdd(w http.ResponseWriter, r *http.Request) {
	conn := getClientConnection(w, r)
	timer := time.NewTimer(5 * time.Second)

	for {
		select {
		case <-timer.C:
			conn.Close()
			return
		default:
			t.counter++
			conn.WriteMessage(1, []byte(strconv.Itoa(t.counter)))
			time.Sleep(200 * time.Millisecond)
		}
	}

}

// SubscribeMessage sends set messages to client
func (t *TestServerWebsocket) SubscribeMessage(w http.ResponseWriter,
	r *http.Request) {
	conn := getClientConnection(w, r)
	if t.messages != nil && len(t.messages) > 0 {
		for _, message := range t.messages {
			conn.WriteMessage(1, message)
		}
	}

}

func getClientConnection(w http.ResponseWriter, r *http.Request) *websocket.Conn {
	var upgrader = websocket.Upgrader{}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatalf(err.Error())
	}
	return conn
}

// decodeMessages turn serialized messages into objects

func replayWithoutTrades(messages []logs.ReplayMessage, tradeChan, priceChan,
	historyChan chan []byte, stopChan chan struct{}) {
	for i, message := range messages {
		dataToSend := []byte(message.Message)
		globaltime.GetGlobalTime().Set(message.ID)

		if strings.Contains(message.Message, "depth") {
			priceChan <- dataToSend

			msgs := controller.DebugSendUnfilledTrades(globaltime.GetGlobalTime().Get(),
				data.GenerateFilled)
			for _, msg := range msgs {
				tradeChan <- []byte(msg)
			}
		} else if strings.Contains(message.Message, "kline") {
			historyChan <- dataToSend
		} else {
			// 	if len(dataToSend) > 1 {
			// 		data := string(dataToSend)
			// 		fmt.Println(data)
			// 	}
			// 	// tradeChan <- dataToSend
			fmt.Println(message.Message)
		}
		logMessages(i)
	}
	stopChan <- struct{}{}
}

func logMessages(i int) {
	if i%5000 == 0 {
		fmt.Printf("%v messages sent\n", i)
	}
}

// func sendStops(stopChan chan struct{}) {
// 	for i := 0; i < 4; i++ {

// 	}
// }

func replayJustTrades(messages []logs.ReplayMessage, tradeChan, priceChan,
	historyChan chan []byte, stopChan chan struct{}) {
	for i, message := range messages {
		dataToSend := []byte(message.Message)
		globaltime.GetGlobalTime().Set(message.ID)
		if strings.Contains(message.Message, "executionReport") {
			if strings.Contains(message.Message, "\"NEW\"") {
				data.MakeTrade(dataToSend)

			}
			tradeChan <- dataToSend
		} else if strings.Contains(message.Message, "+") {
			fmt.Println(message.Message)
		}
		logMessages(i)

	}
	stopChan <- struct{}{}

}

// use the chanal id to figure out what chanal you need to send to
// use the id to send the data in order of which they came in (could modify
//the time paramiter)
func TestServerConnectionWrapper() (chan []byte, chan []byte, chan []byte,
	chan struct{}) {
	tradeChan := make(chan []byte)
	priceChan := make(chan []byte)
	historyChan := make(chan []byte)
	stopChan := make(chan struct{})
	messages := logs.DecodeMessages("logs.bin")
	go replayWithoutTrades(messages, tradeChan, priceChan, historyChan, stopChan)
	return tradeChan, priceChan, historyChan, stopChan

}
