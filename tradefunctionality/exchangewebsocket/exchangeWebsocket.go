package exchangewebsocket

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/logs"

	"github.com/gorilla/websocket"
)

func SocketLiveTrading(url string, stopChan chan struct{},
) (dataChan chan []byte) {

	dataChan = make(chan []byte)
	connectURL(url, stopChan, dataChan)
	// if listenKey != "" {
	// 	go keepAlive(listenKey)
	// }

	return dataChan
}

func connectURL(url string, stopChan chan struct{}, messageChan chan []byte) {
	connection, _, err := websocket.DefaultDialer.Dial(url, nil)
	fmt.Println(url)

	if err != nil && strings.Contains(err.Error(), "bad handshake") {
		for i := 0; i < 5; i++ {
			time.Sleep(5 * time.Second)
			fmt.Printf("bad handshake reconecting\n")
			connection, _, err = websocket.DefaultDialer.Dial(url, nil)
			if err == nil {
				break
			}
		}
	}
	if err != nil {
		crash.PanicError(err)
	}

	go loop(connection, messageChan, stopChan, url)
}

func dumby(url string, stopChan chan struct{}) {
	connection, _, err := websocket.DefaultDialer.Dial(url, nil)
	crash.PanicError(err)
	go loop(connection, nil, stopChan, url)

}

func SocketBackTesting(tradeChannel, priceChannel, testTradeChan chan []byte,
	stopC chan struct{}, priceMessages [][]byte) {

	go func(messages [][]byte) {
		for _, message := range messages {
			priceChannel <- message
		}
	}(priceMessages)

	go func() {
		for {
			tradeChannel <- <-testTradeChan
		}

	}()

}

// ReplayMessages makes a websocket and sends the message logs to the
// Message Channal
func ReplayMessages(messageChannel chan []byte, messages [][]byte) {
	var testServer TestServerWebsocket
	testServer.messages = messages
	conn := testServer.RunWebsocket(testServer.SubscribeMessage)
	stopC := make(chan struct{})
	go loop(conn, messageChannel, stopC, "")
}

func loop(connetion *websocket.Conn, messageChannel chan []byte,
	stopC chan struct{}, url string) {
	defer func() {
		_ = connetion.Close()
	}()
	for {
		select {
		case <-stopC:
			return
		default:
			_, message, err := connetion.ReadMessage()
			if err != nil {
				fmt.Println(err.Error())
				if url != "" {
					time.Sleep(30 * time.Minute)
					go connectURL(url, stopC, messageChannel)
				}
				// return
			} else {
				// fmt.Println(string(message))
				go func(conn *websocket.Conn) {
					logs.LogMessage(string(message), logs.BinarydebugFile)
				}(connetion)
				if messageChannel != nil {
					messageChannel <- message
				}

			}

		}

	}
}

func keepAlive(listenKey string) {
	for {
		time.Sleep(30 * time.Minute)
		client := &http.Client{}
		url := "https://api.binance.com/api/v1/userDataStream?listenKey=" + listenKey
		req, _ := http.NewRequest("PUT", url, nil)
		_, err := client.Do(req)
		crash.PanicError(err)
		fmt.Printf("keeping user stream open at %v\n", time.Now())
	}

}
