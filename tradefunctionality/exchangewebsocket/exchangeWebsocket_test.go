package exchangewebsocket

import (
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
	"time"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"

	"github.com/gorilla/websocket"
)

func echoMessages(messageChannel chan []byte, stopChan chan struct{}) {
	for {
		select {
		case message := <-messageChannel:
			fmt.Println(string(message))
		case <-stopChan:
			fmt.Println("stoping...")
			return
		}
	}
}
func Test_loop(t *testing.T) {

	messageChan := make(chan []byte)
	stopC := make(chan struct{})
	go echoMessages(messageChan, stopC)
	// var five = (1 * time.Second)
	var server TestServerWebsocket

	type args struct {
		connetion      *websocket.Conn
		messageChannel chan []byte
		stopC          chan struct{}
		waitTime       *time.Duration
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"no clue", args{server.RunWebsocket(server.SubscribeAdd), messageChan, stopC, nil}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			loop(tt.args.connetion, tt.args.messageChannel, tt.args.stopC,"")
		})
	}
}

func TestSocketBackTesting(t *testing.T) {
	type args struct {
		tradeChannel  chan []byte
		priceChannel  chan []byte
		testTradeChan chan []byte
		stopC         chan struct{}
		priceMessages [][]byte
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			SocketBackTesting(tt.args.tradeChannel, tt.args.priceChannel, tt.args.testTradeChan, tt.args.stopC, tt.args.priceMessages)
		})
	}
}

func createFile(fileName string) *os.File {
	file, err := os.Create(fileName)
	crash.PanicError(err)
	return file
}

func DeleteFile(file *os.File) {
	file.Close()
	os.Remove(file.Name())
}

func TestAppendFile(t *testing.T) {
	createdFileName := "fileCreated.txt"
	file := createFile(createdFileName)
	defer DeleteFile(file)
	type args struct {
		fileName string
		text     string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		// {"file created", args{createdFileName, "hello"}},
		{"file not created", args{"fileNotCreated.txt", "hello"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			appendFile(tt.args.fileName, tt.args.text)
		})
	}

}

func Test_appendBytes(t *testing.T) {
	createdFileName := "fileCreated.bin"
	file := createFile(createdFileName)
	smile := string(":)")
	file.WriteString(smile)
	defer DeleteFile(file)
	type args struct {
		fileName string
		data     []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		// TODO: Add test cases.
		{"basic", args{createdFileName, []byte("hello")}, []byte(smile + "hello\n")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			appendBytes(tt.args.fileName, tt.args.data)
			got, err := ioutil.ReadFile(createdFileName)
			if err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("error in test %v read file error %v got:%v wan: %v",
					tt.name, err, got, tt.want)

			}
		})
	}
}

// func TestReplayUsingWebSocket(t *testing.T) {
// 	messages := pricehistory.LoadBinanceWebsocketJSON("../../pricedata/trades.td.txt")
// 	messageChan := make(chan []byte)
// 	type args struct {
// 		messages [][]byte
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 	}{
// 		// TODO: Add test cases.
// 		{"basic trade data", args{messages}},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			ReplayMessages(messageChan, tt.args.messages)
// 		})
// 		var sent [][]byte
// 		for i := 0; i < len(messages); i++ {
// 			select {
// 			case message := <-messageChan:
// 				sent = append(sent, message)
// 			}

// 		}
// 		if reflect.DeepEqual(messages, sent) == false {
// 			t.Errorf("error set messages not same as recived")
// 		}

// 	}
// }
