package trade

import (
	"fmt"
	"sync"
	"time"

	exchange "bitbucket.org/ll01/tradebot/tradefunctionality/Exchange"
	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/exchangewebsocket"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"

	"github.com/shopspring/decimal"
)

type key struct {
	market string
	uuid   string
}

// intervalTrade performs trade actions on a givin interval
func intervalTrade(marketCluster *MarketCluster, connect ConnectionFunc) {

	stopChan := make(chan struct{}, 1)
	defer close(stopChan)
	tradeMessageChan := make(chan []byte)
	defer close(tradeMessageChan)
	priceMessageChan := make(chan []byte)
	defer close(priceMessageChan)
	fmt.Println("connecting")
	tradeMessageChan, priceMessageChan, historyChan, stopChan := connect()

	tradeExtractChan := make(chan *data.Trade)

	defer func() {
		fmt.Println("stoping...")
		close(tradeMessageChan)
		close(priceMessageChan)
		close(historyChan)
		close(stopChan)
		close(tradeExtractChan)
	}()

	fmt.Println("connected")
	lastAssesmentTime := make(map[string]time.Time)
	// var ops uint64
	var priceUpdateWait sync.WaitGroup
	go marketCluster.checkTrades(tradeExtractChan)
	go tradeHandler(marketCluster, tradeExtractChan, 2*time.Hour)
	for {
		select {
		case messageRaw := <-priceMessageChan:

			marketID := marketCluster.Exchange.PriceResponse(messageRaw)
			market := marketCluster.individualMarkets[marketID]
			switch marketCluster.Exchange.Base().TestType {
			case exchange.Live:
				priceUpdateWait.Wait()
				currentTime := market.Time()
				nextAssmentTime := lastAssesmentTime[marketID].Add(
					marketCluster.tradeInterval.TimeToWait)
				if currentTime.After(nextAssmentTime) {
					assessMarket(market)
					lastAssesmentTime[marketID] = currentTime
				}
			case exchange.BackTest:
				market.UpdatePriceHistory(marketCluster.tradeInterval)
				assessMarket(market)
			case exchange.ReplayTest:
				assessMarket(market)
			}

		case messageRaw := <-tradeMessageChan:
			priceUpdateWait.Add(1)
			marketCluster.Exchange.TradeResponse(tradeExtractChan, messageRaw)

			priceUpdateWait.Done()
		case messageRaw := <-historyChan:
			id, _, history := marketCluster.Exchange.PriceHistoryResponse(messageRaw)
			if market, ok := marketCluster.individualMarkets[id]; ok {
				market.h = append(market.h[1:], *history)
			}
		case <-stopChan:
			return
		}
	}

}

var runWaitGroup sync.WaitGroup

// Run starts the trading with the type of trading
// interval (basicly day trading)
func Run(clusters []*MarketCluster, name string, isLive bool) {
	runWaitGroup.Add(len(clusters))
	for _, mc := range clusters {
		go mc.startTrading(name, isLive)
	}

	runWaitGroup.Wait()
}

func (mc *MarketCluster) startTrading(name string, isLive bool) {
	var connect ConnectionFunc
	if isLive || mc.Exchange.Base().TestType == exchange.BackTest {
		connect = mc.loopConnection24H
	} else {
		connect = exchangewebsocket.TestServerConnectionWrapper
		mc.Exchange.Base().TestType = exchange.ReplayTest

	}
	switch name {
	case "interval":
		intervalTrade(mc, connect)

	}
	runWaitGroup.Done()
}

// ConnectionFunc the format for the system to hold a connection
type ConnectionFunc func() (trade, price, history chan []byte, stop chan struct{})

func (mc *MarketCluster) loopConnection24H() (trade, price, history chan []byte,
	stop chan struct{}) {
	//10 minutes before 24 hours (streams are only live for 24 hours)
	looper := time.NewTicker(((60 * 24) - 10) * time.Minute)

	connectAll := func(mc *MarketCluster) {
		trade = mc.Exchange.ConnectTrades()
		price = mc.Exchange.ConnectDepth(mc.individualMarkets.GetMarketIDs())
		history = mc.Exchange.ConnectHisory(mc.individualMarkets.GetMarketIDs(), mc.tradeInterval.APIName)
		stop = mc.Exchange.GetStopChan()
	}
	connectAll(mc)
	go func() {
		for {
			<-looper.C
			if globaltime.GetGlobalTime().GetIsDebugging() == 1 {
				fmt.Println("performing recontection")
			}
			connectAll(mc)
		}
	}()

	return trade, price, history, stop
}

func tradeHandler(cluster *MarketCluster, input chan *data.Trade, remove time.Duration) {
	for {
		trade := <-input
		if trade.Status == data.Filled {
			if trade.Side == "sell" {
				cluster.individualMarkets[trade.Market].pricingStrat.Success(trade)
				controller.RemoveTrade(trade)
			}
		} else {
			currentTime := globaltime.GetGlobalTime().Get()
			deadline := currentTime.Add(-remove)
			//TODO allow cancle sell trades
			if trade.Time.Before(deadline) && trade.Side == "buy" {
				market := cluster.individualMarkets[trade.Market]
				controller.CancelTrade(trade, market.Exchange.CancelOrder)
				cluster.individualMarkets[trade.Market].pricingStrat.Fail(trade)
			}
		}

	}
}

func assessMarket(market *Market) {
	for _, strategy := range market.Strategies {
		strategy.Update(market.h)
	}
	market.pricingStrat.Update(market.h)
	if market.BuySignal() {
		quantity := market.pricingStrat.CalculateAmount()
		if quantity.Value.LessThanOrEqual(decimal.NewFromFloat(0)) {
			return
		}

		quantity.Value = market.Exchange.CorrectValue(market.MarketString,
			quantity.Value)
		price := market.pricingStrat.CalculatePrice("buy", nil)
		market.BuyTradeCoin(price, quantity.Value)

	}
	if market.SellSignal() {
		// rateAsFloat := controller.GetOrderBook(market.MarketString).GetFirstPrice(
		//controller.Ask)

		profitableTrades := market.pricingStrat.CalculateProfitability(
			controller.GetAllTrades())
		for _, trade := range profitableTrades {
			price := market.pricingStrat.CalculatePrice("sell", &trade)
			market.SellTradeCoin(price, trade.Amount, trade.ID)
			controller.RemoveTrade(&trade)
		}
	}
	if globaltime.GetGlobalTime().GetIsDebugging() == 3 {
		fmt.Println(market.MarketString)
		controller.PrintMap()
		fmt.Printf("API calls %v \n", crash.GetAPICall())
	}

}
