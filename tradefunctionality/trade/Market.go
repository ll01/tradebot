package trade

import (
	"time"

	exchange "bitbucket.org/ll01/tradebot/tradefunctionality/Exchange"
	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/settings"
	pricing "bitbucket.org/ll01/tradebot/tradefunctionality/strategy/Pricing"
	trading "bitbucket.org/ll01/tradebot/tradefunctionality/strategy/Trading"

	"github.com/shopspring/decimal"
)

type Side int

const (
	sell Side = iota
	buy
)

//Market trade struct to handle trade manipulation
type Market struct {
	currencyCode string
	MarketString string
	Exchange     exchange.Exchange
	SellCounter  int64
	h            pricehistory.HistoricPrices
	Strategies   trading.Strategies
	pricingStrat pricing.PriceStrategy
	StartAmount  decimal.Decimal
}

//NewMarket constructor for market struct
func NewMarket(currencyCode, marketString string, amount decimal.Decimal,
	strategies trading.Strategies, p pricing.PriceStrategy, exchange exchange.Exchange) *Market {

	var newMarket Market
	newMarket.currencyCode = currencyCode
	newMarket.StartAmount = amount
	newMarket.MarketString = marketString
	newMarket.Exchange = exchange
	newMarket.SellCounter = 1
	newMarket.pricingStrat = p
	newMarket.Strategies = strategies.Copy()

	exchange.SetUpOrderBook(newMarket.MarketString)

	return &newMarket
}

//BuyTradeCoin buys crypto from exchange
func (market *Market) BuyTradeCoin(rate, amount decimal.Decimal) string {
	var uuid string
	var err error
	if uuid, err = market.Exchange.BuyLimit(market.MarketString, amount, rate); err == nil {
		currentTime := market.Time()
		controller.CreateTrade(uuid, market.MarketString, "buy", "", rate, amount,
			&currentTime)
	} else {
		crash.PanicError(err)
	}
	return uuid
}

//SellTradeCoin sells crypto from exchange
func (market *Market) SellTradeCoin(rate, amount decimal.Decimal, ref string) string {
	var uuid string
	var err error

	// sells bitcoin therefore buy Ethereum
	if uuid, err = market.Exchange.SellLimit(market.MarketString, amount, rate); err == nil {
		currentTime := market.Time()

		controller.CreateTrade(uuid, market.MarketString, "sell", ref, rate, amount,
			&currentTime)
	} else {
		crash.PanicError(err)
	}
	return uuid
}

// UpdatePriceHistory updates the history for the market
func (market *Market) UpdatePriceHistory(i *settings.Interval) {

	// intervalArray := data.GetOrderBook(market.MarketString).PriceList
	// currentTime := market.Exchange.Base().GetTime()
	// //TODO: ATTUALLY FIX THIS SO THAT IT IS IN SYNC
	// //TODO: THIS IS PROBABLY PROKEN SO DOING HARD CODE PATCH
	// if len(intervalArray) > 0 {
	// 	open := intervalArray[0]
	// 	close := intervalArray[len(intervalArray)-1]
	// 	high := analysis.Max(intervalArray)
	// 	low := analysis.Min(intervalArray)
	// 	temp := pricehistory.NewHistoricPrice(open, close, high, low, currentTime)
	// 	market.h = append(market.h, temp)
	// 	data.GetOrderBook(market.MarketString).PriceList = intervalArray[:0]
	// }
	history, err := market.Exchange.GetPriceHistory(market.MarketString, i.APIName)
	crash.PanicError(err)
	market.h = history

}

// BuySignal checks if all strategys in array are ready to buy
func (market *Market) BuySignal() (isBuying bool) {
	isBuying = true
	for _, strategy := range market.Strategies {
		signal := strategy.BuySignal()
		// if signal == true {
		// 	fmt.Println("hi")
		// }
		isBuying = isBuying && signal
	}
	return isBuying
}

//SellSignal checks if all strategys in array are ready to sell
func (market *Market) SellSignal() (isBuying bool) {
	isBuying = true
	for _, strategy := range market.Strategies {
		signal := strategy.SellSignal()
		isBuying = isBuying && signal
	}
	return isBuying
}

func (market *Market) x(isbuying bool) (hasAction bool) {
	for _, strategy := range market.Strategies {
		var resultFromStrat bool
		if isbuying {
			resultFromStrat = strategy.BuySignal()
		} else {
			resultFromStrat = strategy.SellSignal()
		}
		//resultFromStrat = isbuying ? strategy.BuySignal() : strategy.SellSignal()
		hasAction = hasAction && resultFromStrat
	}
	return hasAction
}

// StartIndex returns when to start trading
// (some strategys need to wait till enough data is avalible to be effective)
func (market *Market) StartIndex() int {
	startindex := 0
	for _, strategy := range market.Strategies {
		if startindex < strategy.GetBase().GetStartIndex() {
			startindex = strategy.GetBase().GetStartIndex()
		}
	}
	return startindex
}

// go newBook.Loop(quit, market.Interval.CreateTicker(), market.UpdatePriceHistory)

func (market *Market) Time() time.Time {
	return globaltime.GetGlobalTime().Get()
}

//CorrectAmount some exchanges have rules on the ammount sent this corrects it
func (market *Market) CorrectAmount(amount decimal.Decimal) decimal.Decimal {
	return market.Exchange.CorrectValue(market.MarketString, amount)
}
