package trade

import (
	"fmt"
	"testing"
	"time"
)

// func TestCheckEntryPoints(t *testing.T) {
// 	type args struct {
// 		tradelist *MarketCluster
// 		set       *settings.Settings
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			CheckEntryPoints(tt.args.tradelist, tt.args.set)
// 		})
// 	}
// }
// var firstTime = time.Now()
var addTime = time.Second * 15

func add(time time.Time, duration time.Duration) time.Time {
	return time.Add(duration)
}
func BenchmarkTestAdd(b *testing.B) {
	for n := 0; n < b.N; n++ {
		var firstTime = time.Now()
		add(firstTime, addTime)
	}
}
func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}
func BenchmarkFib10(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		Fib(10)
	}
}

// func Test_calculateSellPrice(t *testing.T) {
// 	type args struct {
// 		makerRate   decimal.Decimal
// 		takerRate   decimal.Decimal
// 		profitRatio decimal.Decimal
// 		buyPrice    decimal.Decimal
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want float64
// 	}{
// 		// TODO: Add test cases.
// 		{"high pressigion",
// 			args{
// 				makerRate:   decimal.NewFromFloat(0.00101),
// 				takerRate:   decimal.NewFromFloat(0.00101),
// 				profitRatio: decimal.NewFromFloat(1.0005),
// 				buyPrice:    decimal.NewFromFloat(0.00030046)},
// 			0.0002997041369925},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got, _ := calculateSellPrice(tt.args.makerRate, tt.args.takerRate, tt.args.profitRatio, tt.args.buyPrice).Float64(); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("calculateSellPrice() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func BenchmarkPrint(b *testing.B) {
	for n := 0; n < b.N; n++ {
		fmt.Printf("hi %v\n", n)
	}
}

// func TestCheckForDeadTrades(t *testing.T) {
// 	trades["hi"] = cre
// 	type args struct {
// 		markets MarketList
// 		remove  time.Duration
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			CheckForDeadTrades(tt.args.markets, tt.args.remove)
// 		})
// 	}
// }
