package trade

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	exchange "bitbucket.org/ll01/tradebot/tradefunctionality/Exchange"
	"bitbucket.org/ll01/tradebot/tradefunctionality/analysis"
	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/settings"
	pricing "bitbucket.org/ll01/tradebot/tradefunctionality/strategy/Pricing"
	trading "bitbucket.org/ll01/tradebot/tradefunctionality/strategy/Trading"

	coinapi_v1 "github.com/CoinAPI/coinapi-sdk/go-rest/v1"
	"github.com/gorjan-mishevski/puzzle"

	"github.com/shopspring/decimal"
)

type MarketList map[string]*Market
type MarketCluster struct {
	individualMarkets MarketList
	Exchange          exchange.Exchange
	tradeInterval     *settings.Interval
}

func createMarketCluster() *MarketCluster {
	var newCluster MarketCluster
	newCluster.individualMarkets = make(map[string]*Market)
	return &newCluster
}

var zero = decimal.NewFromFloat(0.0)

func NewClusterList(currentSettings *settings.Settings) []*MarketCluster {
	var clusters []*MarketCluster
	for id, tradeCoin := range currentSettings.Marketcluster {
		newCluster := createMarketCluster()
		strategies, startIndex := trading.LoadStrategyArray(tradeCoin.TradeStrategy)
		var marketIDs map[string]string
		newInterval := settings.NewIntervalJson(tradeCoin.Interval)
		newCluster.tradeInterval = newInterval

		newCluster.Exchange, marketIDs = exchange.NewExchange(&tradeCoin, startIndex,
			newCluster.tradeInterval.APIName)
		var startAmounts map[string]decimal.Decimal
		if tradeCoin.Exchange.Exchangename != "simcsv" {
			startAmounts = convert(marketIDs, decimal.New(1, 0), cryptoconvert)
		} else {
			startAmounts = make(map[string]decimal.Decimal)
			for _, id := range marketIDs {
				startAmounts[id] = decimal.New(1, 0)
			}
		}

		for tradeCoinCode, marketID := range marketIDs {

			if value, ok := startAmounts[tradeCoinCode]; ok {
				priceStrategy := pricing.LoadStrategy(tradeCoin.Exchange,
					tradeCoin.PriceStrategy, marketID, value, id)
				newCluster.individualMarkets[marketID] = NewMarket(tradeCoinCode,
					marketID, value, strategies,
					priceStrategy, newCluster.Exchange)
			} else {

				fmt.Printf("cant find price for market %s\n", marketID)
			}

		}

		newCluster.SetPriceHistories()
		clusters = append(clusters, newCluster)
	}

	return clusters
}

// GetRequest makes a get request to the given url
func GetRequest(url string) (map[string]interface{}, error) {
	jsonObject := make(map[string]interface{})
	responseData, err := http.Get(url)
	if err == nil {
		defer responseData.Body.Close()
		responseBytes, err := ioutil.ReadAll(responseData.Body)
		crash.PanicError(err)
		json.Unmarshal(responseBytes, &jsonObject)
	} else {
		fmt.Println(err)
	}

	return jsonObject, err
}

func generateCoinURL(tradeCoinCodes map[string]string) string {
	apiEndPoint := "https://api.coinranking.com/v1/public/coins?symbols="
	if len(tradeCoinCodes) <= 50 {
		var coinNames []string
		for coin := range tradeCoinCodes {
			coinNames = append(coinNames, coin)
		}
		apiEndPoint += strings.Join(coinNames, ",")

	} else {
		log.Fatal("monitoring over 50 markets is not suported right now")
	}
	fmt.Println(apiEndPoint)
	return apiEndPoint
}

type conversionFunction func(base string,
	coinCodes map[string]string) map[string]decimal.Decimal

func convert(tradeCoinCodes map[string]string, ammount decimal.Decimal,
	conversion conversionFunction) map[string]decimal.Decimal {
	var baseFiatCurrency = "GBP"
	AltCoinPrices := conversion(baseFiatCurrency, tradeCoinCodes)
	for key, altCoinPrice := range AltCoinPrices {
		startAmount := ammount.Mul(altCoinPrice)
		AltCoinPrices[key] = startAmount
		fmt.Printf("%s : %s\n", key, startAmount.String())
	}
	return AltCoinPrices
}

func coinapiConversion(baseFiatCurrency string,
	coinCodes map[string]string) map[string]decimal.Decimal {
	sdk := coinapi_v1.NewSDK("50E2CB02-4C85-43FD-9139-FAC288306C99")
	rates, err := sdk.Exchange_rates_get_all_current_rates(baseFiatCurrency)
	crash.PanicError(err)
	output := make(map[string]decimal.Decimal)
	for _, rate := range rates {
		if _, ok := coinCodes[rate.Asset_id_quote]; ok == true {
			output[rate.Asset_id_quote] = rate.Rate
		}

	}
	return output
}
func cryptoconvert(base string,
	coinCodes map[string]string) map[string]decimal.Decimal {
	var coins []string
	for tradeCode := range coinCodes {
		coins = append(coins, tradeCode)
	}
	response := puzzle.BaseAgainsMultiPrice(base, coins)
	output := make(map[string]decimal.Decimal)
	for key, value := range response.Rates() {
		output[key] = decimal.NewFromFloat(value)
	}
	return output
}

func (mc *MarketCluster) checkTrades(tradeExtractChan chan *data.Trade) {
	for {
		time.Sleep(60 * 5 * time.Second)
		controller.ManualUpdateCheck(tradeExtractChan, mc.Exchange.CheckOrderCompletion)
	}
}

// ContinueTrading asks wether to continue the program
func (t MarketList) ContinueTrading() bool {
	var continueTrading = true
	for _, trade := range t {
		continueTrading = continueTrading && trade.Exchange.ContinueTrading(trade.MarketString)
	}
	return continueTrading
}

func (t MarketCluster) SetPriceHistories() {
	for _, market := range t.individualMarkets {
		history, err := t.Exchange.GetPriceHistory(market.MarketString,
			t.tradeInterval.APIName)
		crash.PanicError(err)
		market.h = history
	}

}

func (t MarketList) GetMarketIDs() []string {
	var IDS []string
	for _, market := range t {
		IDS = append(IDS, market.MarketString)
	}
	return IDS
}

func (t MarketCluster) Volidity() map[string]float64 {
	output := make(map[string]float64)
	for _, market := range t.individualMarkets {
		output[market.MarketString] = analysis.CalculatePriceVolidity(market.h)
	}
	return output
}
