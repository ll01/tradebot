package trade

import "testing"

// func Test_convert(t *testing.T) {
// 	type args struct {
// 		tradeCoinCodes []string
// 		ammount        decimal.Decimal
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want []decimal.Decimal
// 	}{
// 		// TODO: Add test cases.
// 		{"i dont beleve ", args{[]string{"BTC", "ETH"}, decimal.New(5, 1)}, []decimal.Decimal{decimal.New(5, 1)}},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := convert(tt.args.tradeCoinCodes, tt.args.ammount); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("convert() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func Test_generateCoinURL(t *testing.T) {
	type args struct {
		tradeCoinCodes []string
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"multiple coin codes", args{[]string{"ETH", "USDT", "NEO"}},
			"https://api.coinranking.com/v1/public/coins?symbols=ETH,USDT,NEO"},
		{"single coin codes", args{[]string{"TRX"}},
			"https://api.coinranking.com/v1/public/coins?symbols=TRX"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			coinMap := make(map[string]string)
			for _, coin := range tt.args.tradeCoinCodes {
				coinMap[coin] = "BTC-" + coin
			}
			if got := generateCoinURL(coinMap); got != tt.want {
				t.Errorf("generateCoinURL() = %v, want %v", got, tt.want)
			}
		})
	}
}
