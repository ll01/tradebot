package  controller

import (
	"reflect"
	"testing"
	"time"

	"github.com/shopspring/decimal"
)

const (
	market = "unitTest"
)

func createTestTrade() *Trade {
	return &Trade{ID: "i", Market: market, Rate: decimal.NewFromFloat(1.0),
		Amount: decimal.NewFromFloat(1.0), Side: "buy", Filled: true,
		Reference: "N/A"}
}
func emptyTradeSlice() (slice []*Trade) {
	return slice
}
func TestGetProfitableTrades(t *testing.T) {
	testTrade := createTestTrade()

	trades = make(map[string]*Trade)
	trades[testTrade.ID] = testTrade

	var wantList []*Trade
	wantList = append(wantList, testTrade)

	type args struct {
		market          string
		profitablePrice decimal.Decimal
	}
	tests := []struct {
		name string
		args args
		want []*Trade
	}{
		{
			name: "find single trade simple",
			args: args{
				market:          market,
				profitablePrice: decimal.NewFromFloat(0.5),
			},
			want: emptyTradeSlice()},
		{
			name: "reject expensive trade",
			args: args{
				market:          market,
				profitablePrice: decimal.NewFromFloat(1.1),
			},
			want: wantList},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetProfitableTrades(tt.args.market, tt.args.profitablePrice); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetProfitableTrades() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestManualUpdateCheck(t *testing.T) {
	type args struct {
		statusUpdate updateFilledStatus
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ManualUpdateCheck(tt.args.statusUpdate)
		})
	}
}

func TestGetTradesAfterTime(t *testing.T) {
	testTrade := createTestTrade()
	testTrade.Filled = false
	trades = make(map[string]*Trade)

	trades[testTrade.ID] = testTrade
	type args struct {
		deadline          time.Time
		tradeCreationTime time.Time
	}
	tests := []struct {
		name string
		args args
		want []*Trade
	}{
		// TODO: Add test cases.
		{"trade older than an hour", args{time.Now(), time.Unix(0, 0)}, []*Trade{testTrade}},
		 {"time newer than an minute", args{time.Now(), time.Now().Add(1 * time.Minute)}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			trades[testTrade.ID].time = &tt.args.tradeCreationTime
			if got := GetTradesAfterTime(&tt.args.deadline); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTradesAfterTime() = %v, want %v", got, tt.want)
			}
		})
	}
}
