package controller

import (
	"log"
	"sort"
	"strings"
	"sync"

	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
)

var localBook sync.Map
var lock sync.RWMutex

type LocalOrderBook data.OrderBook
type sideBook data.Book

func SetUp(depth int, isSimulated bool) *LocalOrderBook {
	var newBook LocalOrderBook
	newBook.AskOrderBook.OrderMap = make(map[float64]string)
	newBook.AskOrderBook.Keys = make([]float64, 0)
	newBook.BidOrderBook.OrderMap = make(map[float64]string)
	newBook.BidOrderBook.Keys = make([]float64, 0)
	newBook.Depth = depth
	newBook.IsSim = isSimulated
	return &newBook
}

func (o *LocalOrderBook) Update(price float64, quantity string, orderType data.OrderBookType) {

	lock.Lock()
	defer lock.Unlock()
	bookToUpdate := o.getBook(orderType)
	if o.IsSim == false {
		if strings.Compare(quantity, "0.") == 0 {
			deleteBook(bookToUpdate, price, orderType)
		} else {
			add(bookToUpdate, price, quantity, orderType, o.Depth)
		}

	} else {
		for _, price := range bookToUpdate.Keys {
			deleteBook(bookToUpdate, price, orderType)
		}
		add(bookToUpdate, price, quantity, orderType, o.Depth)
	}
	//TODO fix the price history
	// if orderType == Ask {
	// lock.Lock()
	// o.PriceList = append(o.PriceList, price)
	// defer lock.Unlock()
	// }
}

func (o *LocalOrderBook) Clear(orderType data.OrderBookType) {
	if orderType == data.Ask {
		o.AskOrderBook.OrderMap = make(map[float64]string)
		o.AskOrderBook.Keys = make([]float64, 0)
	} else {
		o.BidOrderBook.OrderMap = make(map[float64]string)
		o.BidOrderBook.Keys = make([]float64, 0)
	}

}

func (o *LocalOrderBook) GetFirstPrice(orderType data.OrderBookType) float64 {
	book := o.getBook(orderType)
	return book.Keys[0]

}

func (o *LocalOrderBook) GetFirstPriceAsOrderBookValue() *data.OrderBookValues {
	lock.RLock()
	defer lock.RUnlock()
	bookAsk := o.getBook(data.Ask)
	bookBid := o.getBook(data.Bid)
	return &data.OrderBookValues{bookAsk.Keys[0], bookBid.Keys[0]}

}

func AddOrderBook(marketID string, newBook *LocalOrderBook) {
	localBook.Store(marketID, newBook)
}

type updateHistoryFunc func(o, c, h, l float64)

func GetOrderBook(id string) *LocalOrderBook {
	result, ok := localBook.Load(id)
	var returnbook *LocalOrderBook
	if ok == true {
		returnbook = result.(*LocalOrderBook)
	} else {
		log.Fatal("trying to get book that is not local " + id)
	}
	return returnbook
}

func (o *LocalOrderBook) AddToPriceList(price float64) {
	lock.Lock()
	defer lock.Unlock()
	o.PriceList = append(o.PriceList, price)
}

func (o *LocalOrderBook) getBook(orderType data.OrderBookType) *data.Book {
	var book *data.Book
	if orderType == data.Ask {
		book = &o.AskOrderBook
	} else {
		book = &o.BidOrderBook
	}
	return book
}

func add(b *data.Book, price float64, quantity string, orderType data.OrderBookType, depth int) {

	b.OrderMap[price] = quantity
	b.Keys = append(b.Keys, price)
	sortBook(b, orderType)
	if depth < len(b.Keys) {
		keysTODelete := b.Keys[depth:]
		for _, key := range keysTODelete {
			delete(b.OrderMap, key)
		}
		b.Keys = b.Keys[:depth]
	}
}

func sortBook(b *data.Book, orderType data.OrderBookType) {

	if orderType == data.Ask {
		sort.Float64s(b.Keys)
	} else {
		sort.Sort(sort.Reverse(sort.Float64Slice(b.Keys)))
	}
}

func deleteBook(b *data.Book, price float64, orderType data.OrderBookType) {
	delete(b.OrderMap, price)
	for i, key := range b.Keys {
		if key == price {
			b.Keys = deleteOutOfOrder(b.Keys, i)
			sortBook(b, orderType)
		}
	}
}

func deleteInOrder(a []float64, i int) []float64 {
	return a[:i+copy(a[i:], a[i+1:])]
}

func deleteOutOfOrder(a []float64, i int) []float64 {
	_, a = a[len(a)-1], a[:len(a)-1]
	return a
}
