package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"sync"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/logs"

	"github.com/shopspring/decimal"
)

// type TradeMap map[string]*data.Trade

var trades sync.Map

var startAmounts map[string]float64

const (
	fileTradesPath = "./trades.bin"
)

//Connect starts the initalizeation process to comunicate with tore
func Connect(isFresh bool) uint {
	// if _, err := os.Stat(fileTradesPath); !os.IsNotExist(err) && !isFresh {

	// 	// DeserializeTradeMap(fileTradesPath)
	// }
	if !isFresh {
		rectify()
	}
	return 0
}

// Save saves the state of the in memory store
func Save() {
	SerializeTradeMap(fileTradesPath)
}

// GetProfitableTrades get list of trade objects that can be sold
func GetProfitableTrades(market string, profitablePrice decimal.Decimal) []data.Trade {
	var tradesToSell []data.Trade
	trades.Range(func(key, value interface{}) bool {
		trade := value.(data.Trade)
		if trade.Rate.LessThanOrEqual(profitablePrice) &&
			trade.Market == market && trade.Status == data.Filled &&
			trade.Side == "buy" {
			tradesToSell = append(tradesToSell, trade)

		}
		return true
	})

	// fmt.Printf("trades found: %v\n", tradesToSell)

	return tradesToSell
}

func GetTradesAfterTime(deadline *time.Time) []data.Trade {
	var tradesAfterDeadline []data.Trade
	trades.Range(func(key, value interface{}) bool {
		trade := value.(data.Trade)
		if trade.Time.Before(*deadline) && trade.Status == data.Waiting {
			tradesAfterDeadline = append(tradesAfterDeadline, trade)
		}
		return true
	})

	return tradesAfterDeadline
}

//CancelTrade sends cancel trade to exchange and handals the counters
func CancelTrade(trade *data.Trade, sendCanceToExchange func(market,
	uuid string) error) {
	err := sendCanceToExchange(trade.Market, trade.RawID)
	if err != nil {
		log.Fatalf("error canceling trade %#+v error :%s\n", trade, err.Error())
	}
	if trade.Side == "buy" {

		RemoveTrade(trade)
	} else {
		log.Fatalf("canceling sell trades are not supported yet trade: %#+v\n",
			trade)
	}
	logs.GetLog().UpdateStatus(trade.ID, "cancelled")
}

//CreateTrade creates and adds trade to  be stored
func CreateTrade(id, market, side, ref string, rate, quantity decimal.Decimal,
	time *time.Time) data.Trade {
	newTrade := data.NewTrade("NOT_SET", id, market, side, ref, data.Waiting,
		rate, quantity, time)
	logs.GetLog().InsertTradeIntoTable(&newTrade)
	trades.Store(newTrade.ID, newTrade)
	return newTrade
}

// RemoveTrade deletes trade from store
func RemoveTrade(trade *data.Trade) {
	if globaltime.GetGlobalTime().GetIsDebugging() == 2 {
		fmt.Printf("removing trade %v\n", trade)
	}
	trades.Delete(trade.ID)

}

func LookUpTrade(exchangeID, market string) *data.Trade {
	var result *data.Trade
	trades.Range(func(key, value interface{}) bool {
		trade := value.(data.Trade)
		if trade.RawID == exchangeID && trade.Market == market {
			result = &trade
		}
		return true
	})
	return result
}

// UpdateTradeToFilled changes trade to filled status
func UpdateTradeToFilled(id string) {

	if value, ok := trades.Load(id); ok == true {
		trade := value.(data.Trade)
		trade.Status = data.Filled
		trades.Store(trade.ID, trade)
		logs.GetLog().UpdateStatus(id, data.Filled)
	} else {
		fmt.Println("trade unable to be found with id ", id)
	}
}
func GetAllTrades() []data.Trade {
	var output []data.Trade
	trades.Range(func(key, value interface{}) bool {
		trade := value.(data.Trade)
		output = append(output, trade)
		return true
	})

	return output
}

type updateFilledStatus func(uuid, market string) string

// ManualUpdateCheck uses rest request to ask if trade is filled
func ManualUpdateCheck(extractChan chan *data.Trade, statusUpdate updateFilledStatus) {
	trades.Range(func(key, value interface{}) bool {
		trade := value.(data.Trade)
		if trade.Status == data.Waiting {
			logManualTrade(&trade)
			trade.Status = statusUpdate(trade.Market, trade.RawID)
			log.Printf("Updating status for trade : %#+v\n", trade)
			extractChan <- &trade

		}
		return true
	})

}
func logManualTrade(trade *data.Trade) {
	data := []string{trade.ID, trade.Market, trade.Amount.StringFixedBank(7),
		trade.Rate.StringFixedBank(7), trade.Side}
	msg := "+++++" + strings.Join(data, ";") + "++++"
	logs.LogMessage(msg, logs.BinarydebugFile)
	logs.GetLog().UpdateStatus(trade.ID, "filled")
}

// UpdateAmount trying to fix binance fuck up
func UpdateAmount(id string, newAmount decimal.Decimal) {
	if value, ok := trades.Load(id); ok == true {
		trade := value.(data.Trade)
		if newAmount != trade.Amount {
			trade.Amount = newAmount
			trades.Store(trade.ID, trade)
			fmt.Println("binance fucked up")
		}

	}
}

func DoseExitst(id string) (trade data.Trade, exists bool) {
	value, exists := trades.Load(id)
	if exists {
		trade = value.(data.Trade)
	}
	return trade, exists
}

// //DoseTradeExitst checks if id of trade exists
// func DoseTradeExitst(trade data.Trade) bool {
// 	_, exists := trades.Load(trade.ID)
// 	return exists
// }

func rectify() {
	// TODO
	unsoldIds := logs.GetLog().GetUnsoldTrades()

	// notCompletedSales := logs.GetLog().GetUnFilledSales()

	fmt.Print(unsoldIds)
	// fmt.Print(notCompletedSales)
}

// DebugSendUnfilledTrades DONOT USE UNLESS DEBUGGING
// sends trades that are waiting to be filled
func DebugSendUnfilledTrades(currentTime time.Time,
	GenerateSendMessage func(time.Time, *data.Trade) string) []string {
	var filledMessages []string
	for _, trade := range GetAllTrades() {
		if trade.Status == data.Waiting {
			message := GenerateSendMessage(currentTime, &trade)
			filledMessages = append(filledMessages, message)
		}
	}
	return filledMessages
}

// PrintMap prints out the contents of the trade map
func PrintMap() {
	trades := GetAllTrades()
	fmt.Print("local trades\n==============================================\n")
	if len(trades) == 0 {
		fmt.Println("no open trades")
	} else {
		for _, trade := range trades {
			fmt.Printf("[id: %v : rate: %v, quantity: %v filled: %v]\n", trade.ID,
				trade.Rate.StringFixedBank(9), trade.Amount.StringFixedBank(9),
				trade.Status)

		}
	}

	fmt.Print("==============================================\n")
}

//SerializeTradeMap turns sync map to json map of [string]trade
func SerializeTradeMap(filePath string) {
	output := make(map[string]data.Trade)
	trades.Range(func(key, value interface{}) bool {
		trade := value.(data.Trade)
		id := key.(string)
		output[id] = trade
		return true
	})
	data, err := json.Marshal(output)
	if err != nil {
		log.Fatalf("unable  to marshal trades map error : %v\n", err.Error())
	}
	err = ioutil.WriteFile(filePath, data, 0644)
	if err != nil {
		log.Fatalf("unable to write file to %v error: %v\n", filePath, err.Error())
	}
}

//DeserializeTradeMap turns json map of [string]trade to sync map
func DeserializeTradeMap(filePath string) {
	tradeMapData, err := ioutil.ReadFile(filePath)
	if err != nil {
		//Todo change to crashes
		log.Fatalf("unable to read file to %v error: %v\n", filePath, err.Error())
	}
	var input map[string]data.Trade
	err = json.Unmarshal(tradeMapData, input)
	if err != nil {
		log.Fatalf("unable  to unmarshal trades map error : %v\n", err.Error())
	}
	for key, value := range input {
		trades.Store(key, value)
	}
}
