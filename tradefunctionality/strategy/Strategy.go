package strategy

import (
	"reflect"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"

	"github.com/shopspring/decimal"
)

// Strategy the base struct for all strategies
type Strategy struct {
	name         string
	startIndex   int
	currentPrice *data.OrderBookValues
}

//IInteractions defines the operations for a basic strategy
type IInteractions interface {
	Copy() interface{}
	Update(pricehistory.HistoricPrices)
	GetBase() *Strategy
}

//NewStrategy returns new strategy struct
func NewStrategy(newName string, newStartIndex int, child IInteractions) *Strategy {
	return &Strategy{newName, newStartIndex, nil}
}

// GetName gets name of strategy
func (s *Strategy) GetName() string {
	return s.name
}

// GetStartIndex gets the start index of the strategy
func (s *Strategy) GetStartIndex() int {
	return s.startIndex
}

//GetCurrentPrice returns the current price last set for market
func (s *Strategy) GetCurrentPrice(t data.OrderBookType) decimal.Decimal {
	return decimal.NewFromFloat(s.currentPrice.Get(t))
}

//SetCurrentPrice sets the current price last set for market
func (s *Strategy) SetCurrentPrice(currentPrice *data.OrderBookValues) {
	s.currentPrice = currentPrice
}

// Update users the pricehistory and the trades price/ profit ratio to update indicators
func (s *Strategy) Update(p pricehistory.HistoricPrices) {
	last := p[len(p)-1].Close
	s.currentPrice = &data.OrderBookValues{last, last}
}

//GetStructName returns the name of the struct as a string using reflection
func GetStructName(i interface{}) string {
	name := ""
	if t := reflect.TypeOf(i); t.Kind() == reflect.Ptr {
		name = t.Elem().Name()
	} else {
		name = t.Name()
	}
	return name
}
