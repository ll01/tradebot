package trading

import (
	"math"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"

	"github.com/montanaflynn/stats"
)

// StanderdDeviationStrategy strategy that uses stochastic
// to assess the market
type StanderdDeviationStrategy struct {
	SD         float64
	variance   float64
	mean       float64
	target     float64
	base       *strategy.Strategy
	oscillator Oscillator
}

// NewStanderdDeviationStrategy returns new stochastic strategy
func NewStanderdDeviationStrategy(persistence int, underSold, overBought,
	target float64) *StanderdDeviationStrategy {
	var newStrategy StanderdDeviationStrategy
	name := strategy.GetStructName(newStrategy)
	newStrategy.base = strategy.NewStrategy(name, 0, &newStrategy)
	newStrategy.oscillator = NewOscillator(underSold, overBought, persistence)
	newStrategy.target = target
	return &newStrategy
}

//GetName returns name of strategy
func (strategy *StanderdDeviationStrategy) GetName() string {
	return "StanderdDeviationStrategy"
}

// Update users the pricehistory and the trades price/ profit ratio to update indicators
func (strategy *StanderdDeviationStrategy) Update(
	priceHistory pricehistory.HistoricPrices) {
	strategy.base.Update(priceHistory)
	var err error

	if len(priceHistory) >= 100 {
		priceData := priceHistory.GetPrices()[len(priceHistory)-100:]

		strategy.variance, err = stats.VarP(priceData)
		crash.PanicError(err)
		strategy.SD = math.Sqrt(strategy.variance)
		strategy.mean, err = stats.Mean(priceData)
		crash.PanicError(err)

	}
}

//BuySignal desides when is the right time to submit a buy request
func (strategy *StanderdDeviationStrategy) BuySignal() bool {

	currentClose, _ := strategy.base.GetCurrentPrice(data.Ask).Float64()
	a := currentClose - strategy.mean
	value := a / strategy.SD

	value1 := (strategy.mean - (strategy.SD * value)) / strategy.mean
	// fmt.Printf("sd : %f, value %f x: %f\n", strategy.SD, value, value1)

	// return strategy.oscillator.ShortTermPersistanceStatus(
	// 	value1) == overBought
	return value1 > strategy.target
}

//SellSignal desides when is the right time to submit a sell request
func (strategy *StanderdDeviationStrategy) SellSignal() bool {
	var isSellable = false
	currentClose, _ := strategy.base.GetCurrentPrice(data.Ask).Float64()
	var value = currentClose / (strategy.mean + strategy.SD)

	if strategy.oscillator.hasOverBought {
		isSellable = strategy.oscillator.shortTermStatus(
			value) == overBought
	} else {
		isSellable = true
	}
	return isSellable
}

// GetBase Returns base class for strategy's
func (strategy *StanderdDeviationStrategy) GetBase() *strategy.Strategy {
	return strategy.base
}

// Copy deep
func (strategy *StanderdDeviationStrategy) Copy() interface{} {

	newStrat := *strategy
	newStrat.oscillator = NewOscillator(
		newStrat.oscillator.UnderSold, newStrat.oscillator.OverBought,
		strategy.oscillator.PersistenceLength)

	return &newStrat
}
