package trading

//Status Enum for the current status of the market
type Status int

const (
	underSold Status = iota
	overBought
	none
)

// Oscillator Parent Struct to all oscillators
type Oscillator struct {
	UnderSold         float64
	OverBought        float64
	PersistenceLength int
	hasOverBought     bool
	ShortTerm         Status
	longTerm          Status
	statusStrength    float64
	lastStatus        []Status
}

// NewOscillator constructor for Oscillator type
func NewOscillator(underSold, overBought float64, persistence int) Oscillator {
	var Oscillator Oscillator
	Oscillator.UnderSold = underSold

	Oscillator.PersistenceLength = persistence
	if overBought <= 0 {
		Oscillator.hasOverBought = false
	} else {
		Oscillator.OverBought = overBought
		Oscillator.hasOverBought = true
	}
	return Oscillator
}

//shortTermStatus returns the short term status of the market
func (o *Oscillator) shortTermStatus(indicator float64) Status {
	var output = none
	if o.hasOverBought {
		if indicator >= o.OverBought {
			output = overBought
		}
	}
	if indicator <= o.UnderSold {
		output = underSold
	}
	return output
}

//ShortTermPersistanceStatus returns the short term status of the market
func (o *Oscillator) ShortTermPersistanceStatus(indicator float64) Status {
	var output = none
	currentStatus := o.shortTermStatus(indicator)
	o.lastStatus = append(o.lastStatus, currentStatus)
	// fmt.Printf("indecator %v\npersistence %v\n", indicator, o.lastStatus)
	if len(o.lastStatus) >= int(o.PersistenceLength) &&
		AllSame(o.lastStatus[len(o.lastStatus)-int(o.PersistenceLength):]) &&
		o.lastStatus[len(o.lastStatus)-int(o.PersistenceLength)] != none {
		output = o.lastStatus[len(o.lastStatus)-int(o.PersistenceLength)]

	}
	// fmt.Println(indicator)
	return output
}

// AllSame checks wether all stateses in the list are the same
func AllSame(list []Status) bool {
	var output bool
	first := list[0]
	for _, status := range list {
		if first == status {
			output = true
		} else {
			return false
		}
	}
	return output
}
