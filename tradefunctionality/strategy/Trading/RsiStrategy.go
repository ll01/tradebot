package trading

import (
	"bitbucket.org/ll01/tradebot/tradefunctionality/analysis"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"
)

//GetHistory definition of a exchane that has a history fuction
var GetHistory func(market, tickInterval string) (pricehistory.HistoricPrices, error)

// RsiTrade stategy that uses the relative
// strength index to assess the market
type RsiTrade struct {
	oscillator   Oscillator
	indicator    float64
	perioudCount int
	base         *strategy.Strategy
}

// newRsiStrategy retuns new rsi strategy
func newRsiStrategy(overbought, undersold float64, periodCount int) *RsiTrade {
	var newStrategy RsiTrade
	name := strategy.GetStructName(newStrategy)
	newStrategy.base = newTradeStrategy(name, periodCount, &newStrategy)
	newStrategy.oscillator = NewOscillator(undersold, overbought, 1)
	return &newStrategy
}

//GetName Retuns name of strategy
func (strategy *RsiTrade) GetName() string {
	return "RsiTrade"
}

// Update users the pricehistory and the trades price/ profit ratio to update indicators
func (strategy *RsiTrade) Update(priceHistory pricehistory.HistoricPrices) {
	strategy.base.Update(priceHistory)
	strategy.indicator = analysis.CalculateRSI(strategy.perioudCount, priceHistory)
}

//BuySignal desides when is the right time to submit a buy request
func (strategy *RsiTrade) BuySignal() bool {
	return strategy.oscillator.shortTermStatus(strategy.indicator) == overBought
}

//SellSignal desides when is the right time to submit a sell request
func (strategy *RsiTrade) SellSignal() bool {
	return strategy.oscillator.shortTermStatus(strategy.indicator) == underSold
}

// GetBase Returns base class for strategys
func (strategy *RsiTrade) GetBase() *strategy.Strategy {
	return strategy.base
}

//Copy deep
func (strategy *RsiTrade) Copy() interface{} {
	newStrat := *strategy
	newStrat.oscillator = NewOscillator(
		newStrat.oscillator.UnderSold, newStrat.oscillator.OverBought,
		newStrat.oscillator.PersistenceLength)
	return &newStrat
}
