package trading

import (
	"fmt"
	"testing"
)

func TestStochasticStrategy_CopymakesNewoccilator(t *testing.T) {
	type fields struct {
		undersold   float64
		oversold    float64
		persistence int
		periodD     int
		periodK     int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
		{"deep copy with append", fields{3, 14, 3, 20, 80}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			strategy := NewStochasticStrategy(
				tt.fields.periodK, tt.fields.periodD, tt.fields.persistence,
				tt.fields.undersold, tt.fields.oversold)
			strategy.oscillator.lastStatus = append(strategy.oscillator.lastStatus, none)
			if got := strategy.Copy(); len(got.(*StochasticStrategy).oscillator.lastStatus) != 0 {
				lengthOfStatusList := len(got.(*StochasticStrategy).oscillator.lastStatus)
				t.Errorf("StochasticStrategy.Copy() = %v, want %v", lengthOfStatusList, 0)
			} else {
				fmt.Println(got)
			}
		})
	}
}
