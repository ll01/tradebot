package trading

import (
	"reflect"
	"testing"
)

func TestOscillator_ShortTermPersistanceStatus(t *testing.T) {
	type fields struct {
		UnderSold   float64
		OverBought  float64
		lastStatus  []Status
		persistence int
	}
	type args struct {
		indicator float64
	}
	type want struct {
		status     Status
		statusList []Status
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   want
	}{
		// TODO: Add test cases.
		{"persistance no statuses", fields{10, 0, []Status{}, 3},
			args{5}, want{none, []Status{underSold}}},
		{"persistance no statuses", fields{10, 20, []Status{}, 3},
			args{15}, want{none, []Status{none}}},
		{"persistance no statuses", fields{10, 20, []Status{}, 3},
			args{30}, want{none, []Status{overBought}}},
		{"persistance not enough underSold", fields{10, 0, []Status{underSold}, 3},
			args{5}, want{none, []Status{underSold, underSold}}},
		{"persistance not enough overbought", fields{10, 20, []Status{overBought}, 3},
			args{30}, want{none, []Status{overBought, overBought}}},
		{"persistance all UnderSold ", fields{10, 0, []Status{underSold}, 2},
			args{5}, want{underSold, []Status{underSold, underSold}}},
		{"persistance all overbought", fields{20, 70, []Status{overBought}, 2},
			args{75}, want{overBought, []Status{overBought, overBought}}},
		{"persistance enough UnderSold ", fields{10, 0, []Status{none, underSold}, 2},
			args{5}, want{underSold, []Status{none, underSold, underSold}}},
		{"persistance enough overbought", fields{20, 70, []Status{none, overBought}, 2},
			args{75}, want{overBought, []Status{none, overBought, overBought}}},
		{"persistance full none", fields{20, 70, []Status{none}, 2},
			args{50}, want{none, []Status{none, none}}},
		// {"persistance non with single status", fields{20, 70, []Status{none, none}, 2}, args{75}, want{none, []Status{overBought}}},
		// {"persistance none with status", fields{20, 70, []Status{overBought, none}, 2}, args{75}, none},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := NewOscillator(tt.fields.UnderSold, tt.fields.OverBought, tt.fields.persistence)
			o.lastStatus = tt.fields.lastStatus
			got := o.ShortTermPersistanceStatus(tt.args.indicator)
			gotStatusList := o.lastStatus
			if got != tt.want.status || !reflect.DeepEqual(gotStatusList, tt.want.statusList) {
				t.Errorf("Oscillator.ShortTermPersistanceStatus() = %v, want %v fail in test %v", got, tt.want, tt.name)
			}
		})
	}
}
