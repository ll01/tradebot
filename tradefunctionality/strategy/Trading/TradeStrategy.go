package trading

import (
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"
)

// TradeStrategy defines how the strategys interact in the system
type TradeStrategy interface {
	BuySignal() bool
	SellSignal() bool
	strategy.IInteractions
}

func newTradeStrategy(name string, start int, child TradeStrategy) *strategy.Strategy {
	return strategy.NewStrategy(name, start, child)
}

//Strategies List Of Trade Strategies
type Strategies []TradeStrategy

// Copy copys the contents of the array to another this is deep
func (s Strategies) Copy() Strategies {
	output := make([]TradeStrategy, len(s))
	for i, strat := range s {
		output[i] = strat.Copy().(TradeStrategy)
	}
	return output
}

// LoadStrategy loads object from json
func LoadStrategy(stratgyInfo map[string]interface{}) TradeStrategy {
	var newStrategy TradeStrategy
	var strategyName = stratgyInfo["name"].(string)
	switch strategyName {
	case "EMANoOverbought":
		var periodCount = int(stratgyInfo["periods"].(float64))
		var threshold = stratgyInfo["threshold"].(float64)
		newStrategy = NewEMANoOverboughtStrategy(periodCount, threshold)
	case "RsiTrade":
		underSold, overBought, periodCount := GetOscillationValues(stratgyInfo)
		newStrategy = newRsiStrategy(overBought, underSold, periodCount)
	case "StochasticStrategy":
		underSold, overBought, _ := GetOscillationValues(stratgyInfo)
		var periodK = int(stratgyInfo["kperiod"].(float64))
		var periodD = int(stratgyInfo["dperiod"].(float64))
		persistence := int(stratgyInfo["persistence"].(float64))
		newStrategy = NewStochasticStrategy(
			periodK, periodD, persistence, underSold, overBought)
	case "StanderdDeviationStrategy":
		underSold, overBought, _ := GetOscillationValues(stratgyInfo)
		persistence := int(stratgyInfo["persistence"].(float64))
		target := stratgyInfo["target"].(float64)
		newStrategy = NewStanderdDeviationStrategy(persistence, underSold, overBought, target)
	}

	return newStrategy
}

//LoadStrategyArray loads array of trade strategys
func LoadStrategyArray(strategyInfoArray []map[string]interface{}) (Strategies,
	int) {
	var output Strategies
	var startPoint int
	for _, strategyJSON := range strategyInfoArray {
		strategy := LoadStrategy(strategyJSON)
		output = append(output, strategy)
		newStartPoint := strategy.GetBase().GetStartIndex()
		if newStartPoint > startPoint {
			startPoint = newStartPoint
		}
	}
	return output, startPoint
}

// GetOscillationValues returns the oversold underbought and periods to
// load a strategy
func GetOscillationValues(strategyInfo map[string]interface{}) (underSold,
	overBought float64, periodcount int) {
	underSold = strategyInfo["undersold"].(float64)

	overBought = strategyInfo["overbought"].(float64)
	if value, ok := strategyInfo["periods"]; ok == true {
		periodcount = value.(int)
	}
	return underSold, overBought, periodcount
}
