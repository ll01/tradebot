package trading

import (
	"bitbucket.org/ll01/tradebot/tradefunctionality/analysis"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"
)

// StochasticStrategy strategy that uses stochastic
// to assess the market
type StochasticStrategy struct {
	stochasticLines *analysis.StochasticLines
	periodD         int
	periodK         int
	base            *strategy.Strategy
	oscillator      Oscillator
}

// NewStochasticStrategy returns new stochastic strategy
func NewStochasticStrategy(periodK, periodD, persistence int, underSold, overBought float64) *StochasticStrategy {
	var newStrategy StochasticStrategy
	var startIndex = (periodD * periodK) + 1
	name := strategy.GetStructName(newStrategy)
	newStrategy.base = strategy.NewStrategy(name, startIndex, &newStrategy)
	newStrategy.periodK = periodK
	newStrategy.periodD = periodD
	newStrategy.oscillator = NewOscillator(underSold, overBought, persistence)
	return &newStrategy
}

//GetName returns name of strategy
func (strategy *StochasticStrategy) GetName() string {
	return "StochasticStrategy"
}

// Update users the pricehistory and the trades price/ profit ratio to update indicators
func (strategy *StochasticStrategy) Update(
	priceHistory pricehistory.HistoricPrices) {
	strategy.base.Update(priceHistory)
	strategy.stochasticLines = analysis.CalculateStochastic(priceHistory,
		int(strategy.periodD), int(strategy.periodK))
	// fmt.Printf("fast line : %.6f \nslow line : %.6f \n",
	// 	strategy.stochasticLines.FastLine, strategy.stochasticLines.SlowLine)

}

//BuySignal desides when is the right time to submit a buy request
func (strategy *StochasticStrategy) BuySignal() bool {
	return strategy.oscillator.ShortTermPersistanceStatus(
		strategy.stochasticLines.SlowLine) == underSold
}

//SellSignal desides when is the right time to submit a sell request
func (strategy *StochasticStrategy) SellSignal() bool {
	var isSellable = false
	if strategy.oscillator.hasOverBought {
		isSellable = strategy.oscillator.shortTermStatus(
			strategy.stochasticLines.SlowLine) == overBought
	} else {
		isSellable = true
	}
	return isSellable
}

// GetBase Returns base class for strategy's
func (strategy *StochasticStrategy) GetBase() *strategy.Strategy {
	return strategy.base
}

// Copy deep
func (strategy *StochasticStrategy) Copy() interface{} {

	newStrat := *strategy
	newStrat.oscillator = NewOscillator(
		newStrat.oscillator.UnderSold, newStrat.oscillator.OverBought,
		strategy.oscillator.PersistenceLength)

	return &newStrat
}
