package trading

import (
	"bitbucket.org/ll01/tradebot/tradefunctionality/analysis"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"
)

// EMANoOverbought stategy that uses the exponental moving
// average to assess the market
type EMANoOverbought struct {
	currentMovingAverage float64
	periodCount          int
	threshold            float64
	base                 *strategy.Strategy
}

//GetName Retuns name of strategy
func (strategy *EMANoOverbought) GetName() string {
	return "EMANoOverbought"
}

// NewEMANoOverboughtStrategy  returns new ema strategy
func NewEMANoOverboughtStrategy(periodCount int, threshold float64) *EMANoOverbought {
	var newStrategy EMANoOverbought
	name := strategy.GetStructName(newStrategy)
	newStrategy.base = newTradeStrategy(name, periodCount, &newStrategy)
	newStrategy.threshold = threshold
	return &newStrategy
}

// Update users the pricehistory and the trades price/ profit ratio to update indicators
func (strategy *EMANoOverbought) Update(priceHistory pricehistory.HistoricPrices) {
	strategy.base.Update(priceHistory)
	strategy.currentMovingAverage, _ =
		analysis.CalculateExponentialMovingAverageFromArray(priceHistory.GetPrices(), strategy.periodCount)

}

//BuySignal desides when is the right time to submit a buy request
func (strategy *EMANoOverbought) BuySignal() bool {
	currentPrice, _ := strategy.base.GetCurrentPrice(data.Bid).Float64()
	// coifficiant := currentPrice / strategy.currentMovingAverage
	// fmt.Printf("moving average %v\ncoifficiant %v\n", strategy.currentMovingAverage, coifficiant)
	return strategy.currentMovingAverage*strategy.threshold > currentPrice
}

//SellSignal desides when is the right time to submit a sell request
func (strategy *EMANoOverbought) SellSignal() bool {
	// currentPrice, _ := strategy.base.currentPrice.Float64()
	//  return strategy.currentMovingAverage*strategy.threshold < currentPrice && strategy.base.SellSignal()
	return true

}

// GetBase Returns base class for strategys
func (strategy *EMANoOverbought) GetBase() *strategy.Strategy {
	return strategy.base
}

//Copy deep
func (strategy *EMANoOverbought) Copy() interface{} {
	return &*strategy
}
