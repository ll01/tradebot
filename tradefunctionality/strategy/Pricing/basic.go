package pricing

import (
	"fmt"
	"math"
	"sync"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/spacer"
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"

	"github.com/shopspring/decimal"
)

var one = decimal.New(1, 0)

const (
	precision = 8
)

// Basic simple profitmargin pricer
type Basic struct {
	priceStack       []decimal.Decimal
	profitRatio      decimal.Decimal
	splitCount       int
	startAmount      decimal.Decimal
	lastBuyTradeTime time.Time
	tempBuyTradeTime time.Time
	maker            decimal.Decimal
	taker            decimal.Decimal
	base             *strategy.Strategy
	marketID         string
	spacer           *spacer.Spacer
	ratioLookupTable map[string]float64
}

// NewBasic Generates new basic pricing strategy
func NewBasic(split, waitTimeSeconds int, profitRatio, maker, taker float64,
	startAmount decimal.Decimal, marketID string, space *spacer.Spacer) *Basic {
	var s Basic
	s.spacer = space
	s.splitCount = split
	s.profitRatio = decimal.NewFromFloat(profitRatio)
	s.startAmount = startAmount
	s.lastBuyTradeTime = time.Unix(0, 0)
	s.tempBuyTradeTime = time.Unix(0, 0)
	s.maker = decimal.NewFromFloat(maker).Add(one)
	s.taker = decimal.NewFromFloat(taker).Add(one)
	s.marketID = marketID
	s.ratioLookupTable = make(map[string]float64)
	name := strategy.GetStructName(s)
	s.base = strategy.NewStrategy(name, 0, &s)
	return &s

}

//CalculatePrice calculates the price to buy at
func (b *Basic) CalculatePrice(side string, t *data.Trade) decimal.Decimal {

	var price decimal.Decimal
	if side == "buy" {
		price = b.base.GetCurrentPrice(data.Bid)
	} else {
		if t != nil {
			price = b.calculateSesettllPrice(t.Rate, 8)
		} else {
			price = b.base.GetCurrentPrice(data.Ask)
		}
	}
	return price
}

//CalculateProfitability shows the trades that need to be sold
func (b *Basic) CalculateProfitability(trades []data.Trade) []data.Trade {
	var output []data.Trade
	for _, trade := range trades {
		if value := b.profitRatio.Mul(trade.Rate); value.LessThanOrEqual(
			b.GetBase().GetCurrentPrice(data.Ask)) && trade.Market == b.marketID &&
			trade.Side == "buy" && trade.Status == data.Filled {
			output = append(output, trade)
			// fmt.Println(output)
		}

	}
	// log.Fatal("not implemented")
	return output
}
func (b *Basic) popPriceStack() decimal.Decimal {
	// https://github.com/golang/go/wiki/SliceTricks
	var value decimal.Decimal
	value, b.priceStack = b.priceStack[len(b.priceStack)-1],
		b.priceStack[:len(b.priceStack)-1]
	return value
}

func (b *Basic) ratioWithTax(ratio decimal.Decimal) decimal.Decimal {
	return ratio.Mul(b.maker).Mul(b.taker)
}
func (b *Basic) ratioWithoutTax(ratio decimal.Decimal) decimal.Decimal {
	return ratio.Div(b.maker.Mul(b.taker))
}

var lock sync.RWMutex

//Success update the state if there is a successful trade
func (b *Basic) Success(trade *data.Trade) {
	lock.Lock()
	defer lock.Unlock()
	var newAmount decimal.Decimal

	if len(b.priceStack) == 0 {
		newAmount = b.profitRatio.Mul(trade.Amount)
	} else {
		tradeN := logn(b.profitRatio, trade.Amount.Div(b.startAmount)) + 1
		currentPrice := b.popPriceStack()
		currentN := logn(b.profitRatio, currentPrice.Div(b.startAmount)) + 1
		high, low := maxMin(tradeN, currentN)

		addition := low / high
		startAmmountFloat, _ := b.profitRatio.Float64()
		newRatio := math.Pow(startAmmountFloat, addition+high)

		b.ratioLookupTable[trade.ID] = newRatio
		newAmount = b.startAmount.Mul(decimal.NewFromFloat(newRatio))
		newAmount = newAmount.RoundBank(8)
	}
	b.priceStack = append(b.priceStack, newAmount)
	println("===============")
	println(trade.Market)
	for _, p := range b.priceStack {
		fmt.Println(p.String())
	}
	println("===============")

	b.spacer.Deccrement()

}

func maxMin(x, y float64) (high, low float64) {
	if x > y {
		high = x
		low = y
	} else {
		high = y
		low = x
	}
	return high, low
}

//CalculateAmount calculates amout to buy
func (b *Basic) CalculateAmount() Amount {
	lock.Lock()
	defer lock.Unlock()
	isok := b.spacer.IsTradeok(b.lastBuyTradeTime,
		globaltime.GetGlobalTime().Get())
	var amount = Amount{decimal.NewFromFloat(0), b.profitRatio}
	if isok {
		if len(b.priceStack) > 0 {
			amount.Value = b.popPriceStack()
		} else {
			amount.Value = b.startAmount
		}

		b.spacer.Increment(b.marketID, globaltime.GetGlobalTime().Get())
		b.tempBuyTradeTime = b.lastBuyTradeTime
		b.lastBuyTradeTime = globaltime.GetGlobalTime().Get()
	}
	amount.Value = amount.Value.RoundBank(precision)
	amount.Ratio = b.ratioWithTax(amount.Ratio)

	return amount
}

//Fail defines what will happen if a trade is faild and needs to be cancled
func (b *Basic) Fail(trade *data.Trade) {
	if trade.Side == "buy" {
		b.spacer.Deccrement()
		b.lastBuyTradeTime = b.tempBuyTradeTime
	}
}

//Copy deep
func (b *Basic) Copy() interface{} {
	new := *b
	stack := append([]decimal.Decimal{}, b.priceStack...)
	new.priceStack = stack
	new.spacer = b.spacer.Copy()
	return &new
}

//Update update the state of the strat before buy
func (b *Basic) Update(priceHistory pricehistory.HistoricPrices) {
	book := controller.GetOrderBook(b.marketID)
	b.base.SetCurrentPrice(book.GetFirstPriceAsOrderBookValue())

}

//GetBase parent
func (b *Basic) GetBase() *strategy.Strategy {
	return b.base
}

func logn(baseDecimal, valueDecimal decimal.Decimal) float64 {
	base, _ := baseDecimal.Float64()
	value, _ := valueDecimal.Float64()
	return math.Log(value) / math.Log(base)
}

func (b *Basic) calculateSesettllPrice(buyPrice decimal.Decimal,
	precision int32) decimal.Decimal {
	sellPrice := buyPrice.Mul(b.maker.Mul(b.taker).Mul(b.profitRatio))
	return sellPrice.Round(precision)

}
