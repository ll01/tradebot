package pricing

import (
	"sync"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/settings"
	"bitbucket.org/ll01/tradebot/tradefunctionality/spacer"
	"bitbucket.org/ll01/tradebot/tradefunctionality/strategy"

	"github.com/shopspring/decimal"
)

type spaces map[int]*spacer.Spacer

var spacerLoacter spaces
var spaceOnce sync.Once

func GetSpace() spaces {
	spaceOnce.Do(func() {
		spacerLoacter = make(map[int]*spacer.Spacer)
	})

	return spacerLoacter
}

// PriceStrategy defines how a market should be priced
type PriceStrategy interface {
	CalculateAmount() Amount
	CalculatePrice(side string, t *data.Trade) decimal.Decimal
	CalculateProfitability([]data.Trade) []data.Trade
	Success(* data.Trade)
	Fail(* data.Trade)
	strategy.IInteractions
}

// Amount Defines the new amounts value and its target profitability
type Amount struct {
	Value decimal.Decimal
	Ratio decimal.Decimal
}

//LoadStrategy loads a price strategy given the settings
func LoadStrategy(ex settings.ExchangeObject, strategyInfo map[string]interface{},
	marketID string, conversionRatio decimal.Decimal, clusterID int) PriceStrategy {
	var newStrategy PriceStrategy
	var strategyName = strategyInfo["name"].(string)
	switch strategyName {
	case "basic":
		split := int(strategyInfo["split"].(float64))
		waitTime := int(strategyInfo["waittimeseconds"].(float64))
		profitRatio := strategyInfo["profitratio"].(float64)
		startPriceBase := decimal.NewFromFloat(strategyInfo["startamount"].(float64))
		startAmount := conversionRatio.Mul(startPriceBase)
		space := GetSpace()[clusterID]
		if space == nil {
			space = spacer.Initalize(split, waitTime, 0)
			GetSpace()[clusterID] = space
		}
		newStrategy = NewBasic(split, waitTime, profitRatio, ex.Makerfee,
			ex.Takerfee, startAmount, marketID, space)
	}
	return newStrategy
}
