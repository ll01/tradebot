package globaltime

import (
	"log"
	"sync"
	"time"
	"bitbucket.org/ll01/tradebot/tradefunctionality/settings"

	"github.com/line/line-bot-sdk-go/linebot"
)

var currentTime *GlobalTime
var timeOnce sync.Once
var settingObject *settings.Settings

func GetGlobalTime() *GlobalTime {
	timeOnce.Do(func() {
		currentTime = NewGlobalTime(settingObject)
	})
	return currentTime
}
func Init(setting *settings.Settings) {
	settingObject = setting

}

func NewGlobalTime(setting *settings.Settings) *GlobalTime {
	var globalTime GlobalTime
	globalTime.isLive = setting.IsLive
	if setting.IsLive == true {
		globalTime.currentTime = time.Now().UTC()
	} else {
		globalTime.currentTime = time.Unix(0, 0)
	}
	globalTime.DebuggingLevel = setting.DebuggingLevel

	return &globalTime
}

var lock sync.RWMutex

func (g *GlobalTime) Set(newTimeUnix int64) {
	if g.isLive == false || settingObject.IsSimming() {
		lock.Lock()
		defer lock.Unlock()
		g.currentTime = time.Unix(newTimeUnix, 0)
	} else {
		log.Fatal("shoud not be setting global time in live")
	}
}

func (g *GlobalTime) Get() time.Time {
	var currentTime time.Time
	if g.isLive && !settingObject.IsSimming() {
		currentTime = time.Now().UTC()
	} else {
		lock.RLock()
		defer lock.RUnlock()
		currentTime = g.currentTime
	}
	return currentTime
}

func (g *GlobalTime) GetIsDebugging() int {
	return g.DebuggingLevel
}

type GlobalTime struct {
	currentTime    time.Time
	isLive         bool
	DebuggingLevel int
	bot            *linebot.Client
}
