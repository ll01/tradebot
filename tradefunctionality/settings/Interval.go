package settings

import (
	"time"
)

/*Interval object that handels the wait times and the api call names for
units of time */
type Interval struct {
	//APIName name of the interval length int the exchanges api
	APIName    string
	IsDefault  bool
	TimeToWait time.Duration
}

type IntervalJson struct {
	Name     string `json:"name"`
	WaitTime int    `json:"waittime"`
}

//NewInterval creates a new ineval object
func NewInterval(apiName string, TimeToWait time.Duration) *Interval {
	var newInterval Interval
	newInterval.APIName = apiName
	newInterval.TimeToWait = TimeToWait
	return &newInterval
}

func NewIntervalJson(i IntervalJson) *Interval {

	var wait = time.Second * time.Duration(i.WaitTime)
	return NewInterval(i.Name, wait)

}

// // NewIntervalFromTradeSettings loads Interval from settings json
// func NewIntervalFromTradeSettings(currencySetting *MarketCluster) *Interval {
// 	var time = time.Duration(int64(currencySetting.Intervalwaittime)) * time.Second
// 	return NewInterval(currencySetting.Intervalname, time)
// }

//Wait uses time.sleep to wait for the time  parameter
func (i *Interval) Wait() {
	time.Sleep(i.TimeToWait)
}

func (i *Interval) CreateTicker() *time.Ticker {
	return time.NewTicker(i.TimeToWait)
}
