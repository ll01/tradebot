package settings

import (
	"io/ioutil"
	"strings"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"

	jsoniter "github.com/json-iterator/go"
)

// Settings json object that hold set up parameters
type Settings struct {
	Databasename   string          `json:"databasename"`
	IsLive         bool            `json:"islive"`
	IsFresh        bool            `json:"isfresh"`
	DebuggingLevel int             `json:"Debugging"`
	Marketcluster  []MarketCluster `json:"marketcluster"`
	LineSecret     string          `json:"lineSecret"`
	LineToken      string          `json:"lineToken"`
}

//MarketCluster a group of markets with the same settings
type MarketCluster struct {
	Basecoin string `json:"basecoin"`
	// StartAmount   float64                  `json:"startamount"`
	Tradecoins    []string                 `json:"tradecoins"`
	TradeStrategy []map[string]interface{} `json:"tradestrategy"`
	PriceStrategy map[string]interface{}   `json:"pricestrategy"`
	Interval      IntervalJson             `json:"interval"`
	Exchange      ExchangeObject           `json:"exchange"`
}

// ExchangeObject define the parameters for an exchange
type ExchangeObject struct {
	Apikey       string  `json:"apikey"`
	Apisecret    string  `json:"apisecret"`
	Exchangename string  `json:"exchangename"`
	Makerfee     float64 `json:"makerfee"`
	Takerfee     float64 `json:"takerfee"`
}

// Setup loads a settings profile in json defined by the settings path
func Setup(settingsFilePath string) *Settings {
	var settings Settings
	data, err := ioutil.ReadFile(settingsFilePath)
	crash.PanicError(err)
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	err = json.Unmarshal(data, &settings)
	crash.PanicError(err)
	return &settings
}

func (s *Settings) IsSimming() bool {
	isSim := false
	for _, m := range s.Marketcluster {
		if strings.Contains(m.Exchange.Exchangename, "sim") {
			isSim = true
		}
	}
	return isSim
}
