package exchange

import (
	"bytes"
	"encoding/gob"
	"sync/atomic"

	"log"
	"strings"
	"sync"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"

	"github.com/shopspring/decimal"
)

var salemessage uint64
var buymessage uint64
var call = 0

// SimulatedExtchange object used to create a fake exchange to do
// testing on
type SimulatedExtchange struct {
	CurrentTradeIndex     map[string]int
	SimulatedPriceHistory map[string]pricehistory.HistoricPrices
	base                  *BaseExchange
	priceChannel          chan []byte
	tradeChannel          chan []byte
	historyChannel        chan []byte
	stopChannel           chan struct{}
}

var lock = sync.RWMutex{}

// var Wg sync.WaitGroup

func NewPriceSimulationWithMultipleMarkets(
	histories map[string]pricehistory.HistoricPrices, maker, taker float64,
	startIndex int) *SimulatedExtchange {

	var exchange SimulatedExtchange
	exchange.SimulatedPriceHistory = histories
	exchange.CurrentTradeIndex = make(map[string]int)
	for key := range exchange.SimulatedPriceHistory {
		exchange.CurrentTradeIndex[key] = int(startIndex)
	}
	exchange.base = NewBaseExchange(maker, taker, BackTest)
	exchange.priceChannel = make(chan []byte)
	exchange.tradeChannel = make(chan []byte)
	exchange.historyChannel = make(chan []byte)
	exchange.stopChannel = make(chan struct{})
	go exchange.exicute()
	return &exchange
}

func (exchange *SimulatedExtchange) SetStartPoints() {
	// exchange.CurrentTradeIndex = startPoints
}

//BuyLimit sends Limit order to buy trade currency given the quantity and the rate to buy at
func (exchange *SimulatedExtchange) BuyLimit(marketString string, quantity,
	rate decimal.Decimal) (string, error) {

	uuidString := data.GenerateUUID()

	atomic.AddUint64(&buymessage, 1)
	go func(uuid, marketID string) {
		newData := []string{uuidString, marketString, "buy"}
		exchange.tradeChannel <- []byte(strings.Join(newData, ","))
	}(uuidString, marketString)
	return uuidString, nil
}

// SellLimit sends Limit order to sell trade currency given the quantity and the rate to sell at
func (exchange *SimulatedExtchange) SellLimit(marketString string, quantity,
	rate decimal.Decimal) (string, error) {
	uuidString := data.GenerateUUID()
	atomic.AddUint64(&salemessage, 1)
	go func(uuid, marketID string) {
		newData := []string{uuidString, marketString, "sell"}
		exchange.tradeChannel <- []byte(strings.Join(newData, ","))
	}(uuidString, marketString)
	return uuidString, nil
}

// CheckOrderCompletion with the market and uuid it checks wether the trade has been
// susess exchange.curretTimefuly bought/sold
func (exchange *SimulatedExtchange) CheckOrderCompletion(marketString, uuid string) string {
	return data.Filled
}

// CancelOrder ccancel trades that are still open
func (exchange *SimulatedExtchange) CancelOrder(marketString, uuid string) error {
	return nil
}

//GetPriceHistory gets price history of market at a rate of the tick Interval
func (exchange *SimulatedExtchange) GetPriceHistory(marketString,
	tickInterval string) (pricehistory.HistoricPrices, error) {
	var pricesToReturn pricehistory.HistoricPrices
	pricesToReturn = exchange.SimulatedPriceHistory[marketString][:exchange.CurrentTradeIndex[marketString]]
	return pricesToReturn, nil
}

//GetCurrentPrice Gets the most current price for a coin
func (exchange *SimulatedExtchange) GetCurrentPrice(marketString string) (bid,
	ask decimal.Decimal, err error) {
	lock.Lock()
	defer lock.Unlock()
	if exchange.CurrentTradeIndex[marketString] < len(
		exchange.SimulatedPriceHistory[marketString]) {
		var currentPriceData = exchange.SimulatedPriceHistory[marketString][exchange.CurrentTradeIndex[marketString]]
		exchange.CurrentTradeIndex[marketString]++
		globaltime.GetGlobalTime().Set(currentPriceData.TimeOfPrice.Unix())
		return decimal.NewFromFloat(currentPriceData.Close),
			decimal.NewFromFloat(currentPriceData.Close), nil

	}
	panic("exceeded trading data (you have used all the infomation there is to give reset currentTradeIndex to rewind) ")

}

// CreateMarketString generates market string for given coin codes
func (exchange *SimulatedExtchange) CreateMarketString(baseCoinCode,
	tradeCoinCode string) string {
	return tradeCoinCode + baseCoinCode
}

// ContinueTrading asks wether to continue the program
func (exchange *SimulatedExtchange) ContinueTrading(market string) bool {
	var continueTrading = true
	if exchange.CurrentTradeIndex[market] >= len(
		exchange.SimulatedPriceHistory[market])-1 {
		continueTrading = false
	}
	return continueTrading
}

// CorrectValue if the exchange has a minimum increment it truncates
//  the value so it is accepted#
func (exchange *SimulatedExtchange) CorrectValue(market string,
	quantity decimal.Decimal) decimal.Decimal {
	return quantity
}

// GetTime returns Current Time
func (exchange *SimulatedExtchange) GetTime(market string) time.Time {
	// fmt.Println(exchange.CurrentTradeIndex[market])
	return exchange.SimulatedPriceHistory[market][exchange.CurrentTradeIndex[market]].TimeOfPrice
}

//Base Returns base class for exchange
func (exchange *SimulatedExtchange) Base() *BaseExchange {
	return exchange.base
}
func (exchange *SimulatedExtchange) SetUpOrderBook(market string) {
	orderBook := controller.SetUp(10, true)
	controller.AddOrderBook(market, orderBook)
}

func (exchange *SimulatedExtchange) Connect(markets []string, intervalName string,
) (chan []byte, chan []byte, chan []byte, chan struct{}) {
	//save streams to send to
	exchange.priceChannel = make(chan []byte)
	exchange.tradeChannel = make(chan []byte)
	exchange.historyChannel = make(chan []byte)
	exchange.stopChannel = make(chan struct{})

	go exchange.exicute()
	return exchange.tradeChannel, exchange.priceChannel, exchange.historyChannel, exchange.stopChannel
}

func (exchange *SimulatedExtchange) ConnectTrades() chan []byte {

	return exchange.tradeChannel
}

func (exchange *SimulatedExtchange) ConnectDepth(markets []string) chan []byte {
	return exchange.priceChannel
}

func (exchange *SimulatedExtchange) ConnectHisory(markets []string, intervalName string) chan []byte {

	baseHistoryUpdateURL := "wss://stream.binance.com:9443/stream?streams="
	for _, trade := range markets {
		baseHistoryUpdateURL += strings.ToLower(trade) + "@kline_" + intervalName + "/"
	}
	return exchange.historyChannel
}

func (exchange *SimulatedExtchange) PriceResponse(message []byte) string {
	// fmt.Println(string(message))
	marketID := string(message)
	orderBook := controller.GetOrderBook(marketID)
	askAsDecimal, bidAsDecimal, err := exchange.GetCurrentPrice(marketID)
	crash.PanicError(err)
	ask, _ := askAsDecimal.Float64()
	bid, _ := bidAsDecimal.Float64()
	orderBook.Update(ask, "1", data.Ask)
	orderBook.Update(bid, "1", data.Bid)
	orderBook.AddToPriceList(ask)
	// orderBook.PriceList = append(orderBook.PriceList, ask)
	return marketID
}

func (exchange *SimulatedExtchange) TradeResponse(tradeExtractChan chan *data.Trade,
	message []byte) {
	messageAsString := string(message)
	messageData := strings.Split(messageAsString, ",")
	exchangeID := messageData[0]
	market := messageData[1]
	tradeToUpdate := controller.LookUpTrade(exchangeID, market)
	if tradeToUpdate != nil {
		controller.UpdateTradeToFilled(tradeToUpdate.ID)
		tradeExtractChan <- tradeToUpdate
	}

	// fmt.Println(exchange.SimulatedPriceHistory[market][exchange.CurrentTradeIndex[market]].TimeOfPrice)

}

func (exchange *SimulatedExtchange) PriceHistoryResponse(message []byte) (string, string, *pricehistory.HistoricPrice) {
	log.Fatal("not implinmented")

	return "", "", nil
}

func (exchange *SimulatedExtchange) GetStopChan() chan struct{} {

	return exchange.stopChannel
}

func (exchange *SimulatedExtchange) exicute() {

	var sendTradeUpdates = func(marketID string, start, length int) {
		for index := start; index < length; index++ {
			exchange.priceChannel <- []byte(marketID)

		}

	}
	var one sync.WaitGroup
	one.Add(len(exchange.SimulatedPriceHistory))
	for key := range exchange.SimulatedPriceHistory {
		go func(key string) {
			defer one.Done()
			lock.RLock()
			start := exchange.CurrentTradeIndex[key]
			lock.RUnlock()
			length := len(exchange.SimulatedPriceHistory[key]) - 1
			sendTradeUpdates(key, start, length)
		}(key)
	}
	one.Wait()
	exchange.stopChannel <- struct{}{}
}

type testsocketprice struct {
	marketID string
	ask      decimal.Decimal
	bid      decimal.Decimal
}

func (t *testsocketprice) toBytearray() []byte {
	var message bytes.Buffer
	encoder := gob.NewEncoder(&message)
	err := encoder.Encode(t)
	if err != nil {
		log.Fatal(`failed gob Encode`, err)
	}
	return message.Bytes()
}

func byteArrayToPrice(message []byte) testsocketprice {
	var data testsocketprice
	buffer := bytes.Buffer{}
	buffer.Write(message)
	decoder := gob.NewDecoder(&buffer)
	err := decoder.Decode(&data)
	crash.PanicError(err)
	return data
}
