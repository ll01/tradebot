package exchange

//BinanceExchange api calls for Binance
import (
	"bytes"
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"unsafe"

	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	exchangwebsocket "bitbucket.org/ll01/tradebot/tradefunctionality/exchangewebsocket"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"

	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"

	binance "github.com/adshao/go-binance"
	"github.com/buger/jsonparser"
	"github.com/shopspring/decimal"
)

var totalethused float64
var decimalPrecision int32 = 6
var tradeID = 0

type sizeingRules struct {
	StepSize      float64
	TickSize      float64
	minimumAmount float64
}

func newSizeRule(step, tick, minimum float64) sizeingRules {
	var temp sizeingRules
	temp.StepSize = step
	temp.TickSize = tick
	temp.minimumAmount = minimum
	return temp
}

// BinanceExchange  rest client for binance
type BinanceExchange struct {
	client        *binance.Client
	base          *BaseExchange
	stepSize      map[string]sizeingRules
	info          *binance.ExchangeInfo
	TestTradeChan chan []byte
	stop          chan struct{}
	testMessages  [][]byte
}

//NewBinanceExchange makes new BinaceExchange obj ect
func NewBinanceExchange(key, secret string,
	makerFee, takerFee float64, backTradeData [][]byte) *BinanceExchange {
	var binanceExtchange BinanceExchange
	binanceExtchange.client = binance.NewClient(key, secret)
	binanceExtchange.client.Debug = false

	binanceExtchange.stepSize = make(map[string]sizeingRules)
	binanceExtchange.info = nil
	var testType Type
	if backTradeData != nil {

		binanceExtchange.TestTradeChan = make(chan []byte)
		binanceExtchange.testMessages = backTradeData
		testType = ReplayTest
	} else {
		testType = Live
	}
	binanceExtchange.stop = make(chan struct{})
	binanceExtchange.base = NewBaseExchange(makerFee, takerFee, testType)
	return &binanceExtchange
}

func (exchange *BinanceExchange) GetClient() *binance.Client {
	return exchange.client
}

//BuyLimit sends Limit order to buy trade currency given the quantity and the rate to buy at
func (exchange *BinanceExchange) BuyLimit(marketString string, quantity, rate decimal.Decimal) (string, error) {
	// tick := exchange.getTickSize(marketString)
	var buyQuantity = quantity.StringFixedBank(decimalPrecision)
	var buyRate = exchange.round(marketString, rate)
	eth, _ := quantity.Mul(rate).Float64()
	totalethused = +eth
	fmt.Printf("market : %s Current time : %s Quantity : %s Rate : %s side : %s totalEth : %v \n",
		marketString, time.Now().String(), buyQuantity, buyRate, "buy", totalethused)
	order := exchange.client.NewCreateOrderService().Symbol(marketString).Side(binance.SideTypeBuy).
		Type(binance.OrderTypeLimit).TimeInForce(binance.TimeInForceTypeGTC).
		Quantity(buyQuantity).Price(buyRate)
	orderID, err := exchange.sendOrder(order, 0)
	crash.ApiCallAdd()
	return orderID, err
}

func (exchange *BinanceExchange) sendOrder(
	order *binance.CreateOrderService, depth int,
	opts ...binance.RequestOption) (orderID string, err error) {
	if exchange.base.TestType == Live {
		var orderResponse *binance.CreateOrderResponse

		start := time.Now().UTC()
		orderResponse, err = order.Do(context.Background())
		elapsed := time.Since(start)
		log.Printf("sending order took %s\n", elapsed)
		if err == nil && orderResponse != nil {
			orderID = strconv.FormatInt(orderResponse.OrderID, 10)
		} else {
			if strings.Contains(err.Error(), "1021") {
				time.Sleep(10 * time.Second)
				depth++
				if depth < 10 {
					o := binance.WithRecvWindow(10000)
					orderID, err = exchange.sendOrder(order, depth, o)
				} else {
					log.Fatal("10 consecutive send where the response is over 10 " +
						"seconds to respond")
				}
			} else {
				crash.PanicError(err)
			}
		}

	} else {
		orderID = strconv.Itoa(tradeID)
		tradeID++
	}
	return orderID, err
}

// SellLimit sends Limit order to sell trade currency given the quantity and the rate to sell at
func (exchange *BinanceExchange) SellLimit(marketString string, quantity, rate decimal.Decimal) (string, error) {
	var sellQuantity = quantity.StringFixedBank(decimalPrecision)
	var sellRate = exchange.round(marketString, rate)
	eth, _ := quantity.Mul(rate).Float64()
	totalethused = -eth
	//TODO: NOCLUE HOW TO FIX
	if globaltime.GetGlobalTime().GetIsDebugging() == 1 {
		fmt.Printf("market : %s Current time : %s Quantity : %s Rate : %s side : %s totalEth : %v \n",
			marketString, time.Now().String(), sellQuantity, sellRate, "sell", totalethused)
	}

	order := exchange.client.NewCreateOrderService().Symbol(marketString).Side(binance.SideTypeSell).
		Type(binance.OrderTypeLimit).TimeInForce(binance.TimeInForceTypeGTC).
		Quantity(sellQuantity).Price(sellRate)

	orderID, err := exchange.sendOrder(order, 0)
	crash.ApiCallAdd()
	return orderID, err
}

var depthCheck = 0

//CheckOrderCompletion with the market and exchangeID it checks wether the trade has been susessfuly bought/sold
func (exchange *BinanceExchange) CheckOrderCompletion(market, exchangeID string) string {

	var status string
	if id, err := strconv.ParseInt(exchangeID, 10, 64); err == nil {
		order, err := exchange.client.NewGetOrderService().Symbol(market).OrderID(id).Do(context.Background())
		//TODO: fix teribal stopgap
		if err != nil {
			if strings.Contains(err.Error(), "1021") {
				if depthCheck < 10 {
					time.Sleep(10 * time.Second)
					depthCancel++
					status = exchange.CheckOrderCompletion(market, exchangeID)
				} else {
					log.Fatal("10 consecutive send where the response is over 10 " +
						"seconds to respond")
				}
			}
		}
		switch order.Status {
		case "FILLED":
			status = data.Filled
		case "NEW":
		case "PARTIALLY_FILLED":
			status = data.Waiting
		default:
			status = data.Cancelled
		}
	}
	crash.ApiCallAdd()
	return status
}

var depthCancel = 0

//CancelOrder ccancel trades that are still open
func (exchange *BinanceExchange) CancelOrder(market, exchangeID string) error {
	var id int64
	var err error
	if id, err = strconv.ParseInt(exchangeID, 10, 64); err == nil {
		_, err = exchange.client.NewCancelOrderService().Symbol(market).OrderID(id).Do(context.Background())
		if err != nil {
			if strings.Contains(err.Error(), "1021") {
				if depthCancel < 10 {
					time.Sleep(10 * time.Second)
					depthCancel++
					exchange.CancelOrder(market, exchangeID)
				} else {
					log.Fatal("10 consecutive send where the response is over 10 " +
						"seconds to respond")
				}
			}
		}

	}
	crash.ApiCallAdd()
	return err
}

//GetPriceHistory gets price history of market at a rate of the tick Interval
func (exchange *BinanceExchange) GetPriceHistory(marketString, tickInterval string) (pricehistory.HistoricPrices, error) {
	klines, err := exchange.client.NewKlinesService().Symbol(marketString).
		Interval(tickInterval).Limit(1000).Do(context.Background())
	var historicPrices pricehistory.HistoricPrices
	for _, k := range klines {
		open, err := strconv.ParseFloat(k.Open, 64)

		crash.PanicError(err)
		close, err := strconv.ParseFloat(k.Close, 64)
		crash.PanicError(err)
		high, err := strconv.ParseFloat(k.High, 64)
		crash.PanicError(err)
		low, err := strconv.ParseFloat(k.Low, 64)
		crash.PanicError(err)
		var temp = pricehistory.NewHistoricPrice(open, close, high, low, time.Unix(k.CloseTime/1000, 0))
		historicPrices = append(historicPrices, temp)
	}
	crash.ApiCallAdd()
	return historicPrices, err
}

//GetCurrentPrice Gets the most current price for a coin
func (exchange *BinanceExchange) GetCurrentPrice(marketString string) (decimal.Decimal, decimal.Decimal, error) {
	priceBook, err := exchange.client.NewListBookTickersService().Symbol(marketString).Do(context.Background())
	if priceBook == nil {
		crash.PanicError(err)
	}
	askAsDecimal, err := decimal.NewFromString(priceBook[0].AskPrice)
	crash.PanicError(err)
	bidAsDecimal, err := decimal.NewFromString(priceBook[0].BidPrice)
	crash.PanicError(err)
	crash.ApiCallAdd()
	return askAsDecimal, bidAsDecimal, err
}

// GetExtchangeInfo gets metadata like minumum trade and increment size from exchange
func (exchange *BinanceExchange) GetExtchangeInfo(marketString string) {
	if exchange.info == nil {
		exchangeInfo, err := exchange.client.NewExchangeInfoService().Do(context.Background())
		crash.ApiCallAdd()
		if exchangeInfo == nil {
			crash.PanicError(err)
		} else {
			exchange.info = exchangeInfo
		}
	}
	for _, symbol := range exchange.info.Symbols {
		if symbol.Symbol == marketString {
			// fmt.Println(symbol.Filters)
			stepSize := symbol.Filters[2]["stepSize"].(string)
			minimumAmount := symbol.Filters[2]["minQty"].(string)
			tickSize := symbol.Filters[0]["tickSize"].(string)

			stepSizeFloat, err := strconv.ParseFloat(stepSize, 64)
			crash.PanicError(err)
			tickSizeFloat, err := strconv.ParseFloat(tickSize, 64)
			crash.PanicError(err)
			minimumAmountFloat, err := strconv.ParseFloat(minimumAmount, 64)
			exchange.stepSize[marketString] = newSizeRule(stepSizeFloat,
				tickSizeFloat, minimumAmountFloat)
		}
	}
}

//CreateMarketString generates market string for given coin codes while colecting
// tradeRule infomation
func (exchange *BinanceExchange) CreateMarketString(baseCoinCode,
	tradeCoinCode string) string {
	var market string
	market = tradeCoinCode + baseCoinCode

	exchange.GetExtchangeInfo(market)
	return market
}

// ContinueTrading asks wether to continue the program
func (exchange *BinanceExchange) ContinueTrading(market string) bool {
	return exchange.base.ContinueTrading()
}

func (exchange *BinanceExchange) SetUpOrderBook(market string) {
	binanceorderBook, err := exchange.client.NewDepthService().Symbol(market).Do(context.Background())
	crash.PanicError(err)
	orderBook := controller.SetUp(10, false)
	exchange.xxx(orderBook, binanceorderBook.Asks, data.Ask)
	bids := *(*[]binance.Ask)(unsafe.Pointer(&binanceorderBook.Asks))
	exchange.xxx(orderBook, bids, data.Bid)
	// TODO this will fuck up if the market strings are in the same format as
	// 2 different exchanges
	controller.AddOrderBook(market, orderBook)
}

func (exchange *BinanceExchange) xxx(orderBook *controller.LocalOrderBook, orderData []binance.Ask, orderType data.OrderBookType) {

	for _, order := range orderData {
		price, err := strconv.ParseFloat(order.Price, 64)
		crash.PanicError(err)
		orderBook.Update(price, order.Quantity, orderType)
	}
}

// CorrectValue if the exchange has a minimum increment it truncates  the value so it is accepted
func (exchange *BinanceExchange) CorrectValue(market string, quantity decimal.Decimal) decimal.Decimal {

	var stepSize = exchange.getStepSize(market)
	//TODO: fail more grasefuly maby a defult
	quantityAsFloat, _ := quantity.Float64()
	var quantityCorrectlySteped = binance.AmountToLotSize(stepSize, 6, quantityAsFloat)
	return decimal.NewFromFloat(quantityCorrectlySteped)
}

func (exchange *BinanceExchange) getStepSize(market string) (
	stepSize float64) {
	if value, ok := exchange.stepSize[market]; ok {
		stepSize = value.StepSize
	} else {
		log.Fatalf("unable to find info for market %v\n", market)
	}

	return stepSize
}

func (exchange *BinanceExchange) getTickSize(market string) (
	tickSize float64) {
	if value, ok := exchange.stepSize[market]; ok {
		tickSize = value.TickSize
	} else {
		log.Fatalf("unable to find info for market %v\n", market)
	}

	return tickSize
}

func (exchange *BinanceExchange) round(market string,
	price decimal.Decimal) string {
	tickSize := exchange.getTickSize(market)
	precision := calculatePrecision(tickSize)
	return price.RoundBank(precision).StringFixedBank(precision)
}
func calculatePrecision(tickSize float64) int32 {
	var precision int32
	for tickSize < 1 {
		tickSize *= 10
		precision++
	}
	return precision
}

//Base Returns base class for exchange
func (exchange *BinanceExchange) Base() *BaseExchange {
	return exchange.base
}

func (exchange *BinanceExchange) TradeResponse(tradeExtractChan chan *data.Trade,
	message []byte) {
	if bytes.Contains(message, []byte("\"FILLED\"")) {
		market, err := jsonparser.GetUnsafeString(message, "s")
		crash.PanicError(err)
		exchangeIDInt, err := jsonparser.GetInt(message, "i")
		exchangeID := strconv.FormatInt(exchangeIDInt, 10)
		crash.PanicError(err)

		tradeToUpdate := controller.LookUpTrade(exchangeID, market)
		if tradeToUpdate != nil {
			controller.UpdateTradeToFilled(tradeToUpdate.ID)
			amountString, err := jsonparser.GetUnsafeString(message, "q")
			crash.PanicError(err)
			amountDecimal, err := decimal.NewFromString(amountString)
			crash.PanicError(err)
			controller.UpdateAmount(tradeToUpdate.ID, amountDecimal)
			tradeExtractChan <- tradeToUpdate
		} else {
			fmt.Printf("error: unable to fetch trade with exchangeID s% in market",
				exchangeID, market)
		}

		// if side == "buy" {
		// 	quantity, err := jsonparser.GetUnsafeString(message, "q")
		// 	crash.PanicError(err)
		// 	controller.UpdateQuanity(tradeid, market, side, quantity)
		// } else {
		// 	if _, ok := controller.DoseExitst(tradeid, market, side); ok == true {
		// 		controller.DeleteSaleAndUpdateCountersRawID(market, tradeid)
		// 	}

		// }
	}
}

func (exchange *BinanceExchange) PriceResponse(message []byte) string {
	streamName, err := jsonparser.GetUnsafeString(message, "stream")
	crash.PanicError(err)
	market := strings.ToUpper(strings.Split(streamName, "@")[0])
	orderBook := controller.GetOrderBook(market)
	parseOrderBook(message, orderBook, data.Ask)
	parseOrderBook(message, orderBook, data.Bid)
	return market
}

func parseOrderBook(message []byte, orderBook *controller.LocalOrderBook, orderType data.OrderBookType) {
	path := "bids"
	if orderType == data.Ask {
		path = "asks"
	}
	// fmt.Println(string(message))
	orderBook.Clear(orderType)
	jsonparser.ArrayEach(message, func(value []byte, dataType jsonparser.ValueType,
		offset int, err error) {

		priceString, err := jsonparser.GetUnsafeString(value, "[0]")
		crash.PanicError(err)
		price, _ := strconv.ParseFloat(priceString, 64)

		quantity, err := jsonparser.GetString(value, "[1]")
		crash.PanicError(err)
		orderBook.Update(price, quantity, orderType)

	}, "data", path)

}

var paths = [][]string{
	[]string{"data", "k", "o"},
	[]string{"data", "k", "c"},
	[]string{"data", "k", "h"},
	[]string{"data", "k", "l"},
	[]string{"data", "k", "t"},
}

func (exchange *BinanceExchange) PriceHistoryResponse(message []byte,
) (market, interval string, newPriceHistory *pricehistory.HistoricPrice) {
	isNew, err := jsonparser.GetBoolean(message, "data", "k", "x")
	crash.PanicError(err)
	if isNew {
		market, _ = jsonparser.GetUnsafeString(message, "data", "s")
		interval, _ = jsonparser.GetUnsafeString(message, "data", "k", "i")

		var open float64
		var close float64
		var high float64
		var low float64
		var klineTime time.Time
		jsonparser.EachKey(message, func(idx int, value []byte, vt jsonparser.ValueType, err error) {
			switch idx {
			case 0:
				openString, _ := jsonparser.ParseString(value)
				open, _ = strconv.ParseFloat(openString, 64)
			case 1:
				closeString, _ := jsonparser.ParseString(value)
				close, _ = strconv.ParseFloat(closeString, 64)
			case 2:
				highString, _ := jsonparser.ParseString(value)
				high, _ = strconv.ParseFloat(highString, 64)
			case 3:
				lowString, _ := jsonparser.ParseString(value)
				low, _ = strconv.ParseFloat(lowString, 64)
			case 4:
				timeUnix, _ := jsonparser.ParseInt(value)
				timeUnix = timeUnix / 1000
				klineTime = time.Unix(timeUnix, 0)

			}
		}, paths...)

		temp := pricehistory.NewHistoricPrice(open, close, high, low, klineTime)
		newPriceHistory = &temp
	}
	return market, interval, newPriceHistory
}
func (exchange *BinanceExchange) ConnectTrades() chan []byte {

	listenKey, err := exchange.client.NewStartUserStreamService().Do(
		context.Background())
	crash.PanicError(err)
	tradeURL := "wss://stream.binance.com:9443/ws/" + listenKey
	return exchangwebsocket.SocketLiveTrading(tradeURL, exchange.stop)

}

func (exchange *BinanceExchange) ConnectDepth(markets []string) chan []byte {
	basePriceUpdateURL := "wss://stream.binance.com:9443/stream?streams="
	appendStringToArrayItems(markets, "@depth5")
	marketsPart := strings.Join(markets, "/")
	return exchangwebsocket.SocketLiveTrading(basePriceUpdateURL+marketsPart, exchange.stop)
}

func (exchange *BinanceExchange) ConnectHisory(markets []string, intervalName string) chan []byte {
	baseHistoryUpdateURL := "wss://stream.binance.com:9443/stream?streams="
	appendStringToArrayItems(markets, "@kline_"+intervalName)
	historyPart := strings.Join(markets, "/")
	return exchangwebsocket.SocketLiveTrading(baseHistoryUpdateURL+historyPart, exchange.stop)
}

func appendStringToArrayItems(arrayToModify []string, appendValue string) {
	for index, item := range arrayToModify {
		arrayToModify[index] = strings.ToLower(item) + appendValue
	}

}
func (exchange *BinanceExchange) GetStopChan() chan struct{} {

	return exchange.stop
}
