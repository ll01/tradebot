package exchange

import (
	"testing"

	"github.com/shopspring/decimal"
)

// var testMessage = []byte(`{"stream":"neoeth@depth5","data":{"lastUpdateId":93279949,"bids":[["0.06131300","1.45000000",[]],["0.06130800","0.33000000",[]],["0.06125200","99.78000000",[]],["0.06125100","21.20000000",[]],["0.06124000","2.10000000",[]]],"asks":[["0.06140500","0.18000000",[]],["0.06143000","1.47000000",[]],["0.06143100","12.58000000",[]],["0.06144400","1.18000000",[]],["0.06146500","99.83000000",[]]]}}`)
// func Test_parseOrderBook(t *testing.T) {
// 	testOrderBook := setUp(5,false)
// 	type args struct {
// 		message   []byte
// 		orderBook *data.OrderBook
// 		orderType data.OrderBookType
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want []float64
// 	}{
// 		// TODO: Add test cases.
// 		{"no clue", args{testMessage,testOrderBook,Bid},[]float64{0.06124,0.06125100,0.06125200,0.06130800,0.06131300} },
// 		{"no clue", args{testMessage,testOrderBook,Bid},[]float64{0.06146500,0.06125100,0.06125200,0.06130800,0.06131300} },

// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			parseOrderBook(tt.args.message, tt.args.orderBook, tt.args.orderType)
// 		})
// 	}
// }

func TestBinanceExchange_round(t *testing.T) {
	type fields struct {
		stepSize sizeingRules
	}
	testSizeMap := make(map[string]sizeingRules)

	type args struct {
		market string
		price  decimal.Decimal
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		// TODO: Add test cases.
		{"8dp", fields{sizeingRules{0, 0.00000001, 0}}, args{"test", decimal.NewFromFloat(1.061313014)}, "1.06131301"},
		{"6dp", fields{sizeingRules{0, 0.000001, 0}}, args{"test", decimal.NewFromFloat(1.061313014)}, "1.061313"},
		{"2dp", fields{sizeingRules{0, 0.01, 0}}, args{"test", decimal.NewFromFloat(1.061313014)}, "1.06"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testSizeMap["test"] = tt.fields.stepSize
			exchange := &BinanceExchange{
				client:        nil,
				base:          nil,
				stepSize:      testSizeMap,
				info:          nil,
				TestTradeChan: nil,
				testMessages:  nil,
			}
			if got := exchange.round(tt.args.market, tt.args.price); got != tt.want {
				t.Errorf("BinanceExchange.round() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculatePrecision(t *testing.T) {
	type args struct {
		tickSize float64
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{"2", args{0.01}, 2},
		{"8", args{ 0.00000001}, 8},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculatePrecision(tt.args.tickSize); got != tt.want {
				t.Errorf("calculatePrecision() = %v, want %v", got, tt.want)
			}
		})
	}
}
