package exchange

import (
	"fmt"
	"log"

	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"

	"github.com/shopspring/decimal"
	bittrex "github.com/toorop/go-bittrex"
)

//BittrexExchange api calls for Bittrex
type BittrexExchange struct {
	client *bittrex.Bittrex
	base   *BaseExchange
}

// NewBittrexExchange creates new bittrexExchange object
func NewBittrexExchange(key, secret string, commitionRate float64) *BittrexExchange {
	var exchange BittrexExchange
	exchange.client = bittrex.New(key, secret)
	exchange.base = NewBaseExchange(commitionRate, commitionRate, Live)
	return &exchange
}

// BuyLimit sends Limit order to buy trade currency given the quantity and the rate to buy at
func (exchange *BittrexExchange) BuyLimit(marketString string, quantity, rate decimal.Decimal) (string, error) {
	fmt.Println("buying " + quantity.StringFixedBank(4) + "at a rate of " + rate.StringFixedBank(4))
	return exchange.client.BuyLimit(marketString, quantity, rate)
}

// SellLimit sends Limit order to sell trade currency given the quantity and the rate to sell at
func (exchange *BittrexExchange) SellLimit(marketString string, quantity, rate decimal.Decimal) (string, error) {
	fmt.Println("selling " + quantity.StringFixedBank(4) + "at a rate of " + rate.StringFixedBank(4))
	return exchange.client.BuyLimit(marketString, quantity, rate)
}

// CheckOrderCompletion with the market and uuid it checks wether the trade has been susessfuly bought/sold
func (exchange *BittrexExchange) CheckOrderCompletion(marketString string, uuid string) string {
	order, err := exchange.client.GetOrder(uuid)
	var status string
	if order.CancelInitiated {
		status = data.Cancelled
	} else {
		if order.IsOpen {
			status = data.Waiting
		} else {
			status = data.Filled
		}
	}
	crash.PanicError(err)
	return status
}

// CancelOrder ccancel trades that are still open
func (exchange *BittrexExchange) CancelOrder(marketString string, uuid string) error {
	fmt.Println("cancleing order with uuid" + uuid)
	return exchange.client.CancelOrder(uuid)
}

//GetPriceHistory gets price history of market at a rate of the tick Interval
func (exchange *BittrexExchange) GetPriceHistory(marketString, tickInterval string) (pricehistory.HistoricPrices, error) {
	candalSticks, err := exchange.client.GetTicks(marketString, tickInterval)
	crash.PanicError(err)
	var prices pricehistory.HistoricPrices
	for _, candal := range candalSticks {
		openFloat, _ := candal.Open.Float64()
		closeFloat, _ := candal.Close.Float64()
		highFloat, _ := candal.High.Float64()
		lowFloat, _ := candal.Low.Float64()
		var temp = pricehistory.NewHistoricPrice(openFloat, closeFloat, highFloat, lowFloat, candal.TimeStamp.UTC())
		prices = append(prices, temp)
	}
	return prices, err
}

//GetCurrentPrice Gets the most current price for a coin
func (exchange *BittrexExchange) GetCurrentPrice(marketString string) (decimal.Decimal, decimal.Decimal, error) {
	ticker, err := exchange.client.GetTicker(marketString)
	crash.PanicError(err)
	return ticker.Ask, ticker.Bid, err
}

//CreateMarketString generates market string for given coin codes
func (exchange *BittrexExchange) CreateMarketString(baseCoinCode, tradeCoinCode string) string {
	return baseCoinCode + "-" + tradeCoinCode
}

// ContinueTrading asks wether to continue the program
func (exchange *BittrexExchange) ContinueTrading(market string) bool {
	return exchange.base.ContinueTrading()
}

// CorrectValue if the exchange has a minimum increment it truncates  the value so it is accepted
func (exchange *BittrexExchange) CorrectValue(market string, quantity decimal.Decimal) decimal.Decimal {
	return quantity
}

func (exchange *BittrexExchange) SetUpOrderBook(market string) *data.OrderBook {
	var goKILLYOURSELFYOUFUCKINGPICEOFSHIT data.OrderBook
	log.Fatal("not implemented")
	return &goKILLYOURSELFYOUFUCKINGPICEOFSHIT
}

//Base Returns base class for exchange
func (exchange *BittrexExchange) Base() *BaseExchange {
	return exchange.base
}
