package exchange

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/data"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
	"bitbucket.org/ll01/tradebot/tradefunctionality/settings"

	"github.com/shopspring/decimal"
)

type Type int

const (
	NullMarket = "N/A"

	BackTest Type = iota
	ReplayTest
	Live
)

//Exchange Defines the api iteraction for a given extchange
type Exchange interface {
	BuyLimit(marketString string, quantity decimal.Decimal, rate decimal.Decimal) (string, error)
	SellLimit(marketString string, quantity decimal.Decimal, rate decimal.Decimal) (string, error)
	CheckOrderCompletion(marketString, uuid string) string
	CancelOrder(market, uuid string) error
	GetPriceHistory(marketString, tickInterval string) (pricehistory.HistoricPrices, error)
	// GetCurrentPrice(marketString string) (decimal.Decimal, decimal.Decimal, error)
	CreateMarketString(baseCoinCode, tradeCoinCode string) string
	ContinueTrading(market string) bool
	CorrectValue(market string, quantity decimal.Decimal) decimal.Decimal
	SetUpOrderBook(market string)
	TradeResponse(tradeExtractChan chan *data.Trade, message []byte)
	PriceResponse(message []byte) string
	PriceHistoryResponse(message []byte) (marketIDstring, intervalID string,
		history *pricehistory.HistoricPrice)
	ConnectTrades() (dataChan chan []byte)
	ConnectDepth(markets []string) (dataChan chan []byte)
	ConnectHisory(markets []string, intervalName string) (dataChan chan []byte)
	GetStopChan() chan struct{}

	Base() *BaseExchange
}

// BaseExchange creates exchange inherited from all
// other exchanges
type BaseExchange struct {
	MakerFee  decimal.Decimal
	TakerFee  decimal.Decimal
	TestType  Type
	isTrading bool
}

// NewBaseExchange creates new base exchange object
func NewBaseExchange(makerFee, takerFee float64, TestType Type) *BaseExchange {
	var base BaseExchange
	base.MakerFee = decimal.NewFromFloat(makerFee)
	base.TakerFee = decimal.NewFromFloat(takerFee)
	base.isTrading = true
	base.TestType = TestType
	return &base
}

// ContinueTrading asks wether to continue the program
func (exchange *BaseExchange) ContinueTrading() bool {
	// if Getspacer().AmountOfAlloudTrades <= 0 {
	// 	exchange.isTrading = false
	// }
	return exchange.isTrading
}

// GetTime returns Current Time
func GenerateMarketIDs(e Exchange, s *settings.MarketCluster) map[string]string {
	marketIDs := make(map[string]string)
	for _, tradeCoin := range s.Tradecoins {
		marketID := e.CreateMarketString(s.Basecoin, tradeCoin)
		marketIDs[tradeCoin] = marketID
	}
	return marketIDs
}

type loadPriceData func() pricehistory.HistoricPrices

//NewExchange loads object from json
func NewExchange(setting *settings.MarketCluster, startIndex int, API string,
) (exchange Exchange, marketIDs map[string]string) {
	e := setting.Exchange
	switch setting.Exchange.Exchangename {
	case "bittrex":
		// exchange = NewBittrexExchange(e.Apikey, e.Apisecret, e.Makerfee)
	case "binance":
		exchange = NewBinanceExchange(e.Apikey, e.Apisecret, e.Makerfee,
			e.Takerfee, nil)
		marketIDs = GenerateMarketIDs(exchange, setting)
	case "sim":
		exchange = NewBinanceExchange(e.Apikey, e.Apisecret, e.Makerfee,
			e.Takerfee, nil)
		marketIDs = GenerateMarketIDs(exchange, setting)
		var justIDs []string
		for _, value := range marketIDs {
			justIDs = append(justIDs, value)
		}

		histories := DownloadPriceHistory(exchange, justIDs, API)
		exchange = NewPriceSimulationWithMultipleMarkets(histories, e.Makerfee,
			e.Takerfee, startIndex)
	case "simcsv":
		root := "./pricedata"
		fileList := findAllCsvFiles(root)
		histories := make(map[string]pricehistory.HistoricPrices)
		marketIDs = make(map[string]string)
		for _, file := range fileList {
			history := pricehistory.ParseCsvFile(file)
			histories[file] = history
			marketIDs[file] = file

		}
		exchange = NewPriceSimulationWithMultipleMarkets(histories, e.Makerfee,
			e.Takerfee, startIndex)

	case "binancetest":
		// data := pricehistory.LoadBinanceWebsocketJSON(`F:\Programming\Go\src\tradebot/test2.txt`)
		// exchange = NewBinanceExchange(e.Apikey, e.Apisecret, e.Makerfee, e.Takerfee, data)
	default:
		log.Fatal("invalid exchange name " + e.Exchangename)

	}

	return exchange, marketIDs
}

type priceHistoryAPICall struct {
	h pricehistory.HistoricPrices
	m string
}

// DownloadPriceHistory gets historic prices for multiple markets
func DownloadPriceHistory(e Exchange, markets []string, API string) map[string]pricehistory.HistoricPrices {
	output := make(map[string]pricehistory.HistoricPrices)
	jobs := make(chan priceHistoryAPICall, len(markets))
	for _, market := range markets {
		go priceHistoryJob(jobs, e, market, API)
	}
	for range markets {
		select {
		case x := <-jobs:
			output[x.m] = x.h
		case <-time.After(10 * time.Second):
			fmt.Println("getting price history timed out")
		}
	}
	return output
} // markets needs to be a map to hold each tickinterval maby
// set the last prich history to the current price

// priceHistoryJob calls the price history function of exchange
func priceHistoryJob(outputChannel chan priceHistoryAPICall, e Exchange, market,
	interval string) {
	result, _ := e.GetPriceHistory(market, interval)
	if result != nil {
		outputChannel <- priceHistoryAPICall{result, market}
	}

}

func findAllCsvFiles(root string) []string {
	var listOfCsvFiles []string
	visit := func(path string, f os.FileInfo, err error) error {
		fmt.Printf("Visited: %s\n", path)
		if !f.IsDir() && filepath.Ext(path) == (".csv") {
			listOfCsvFiles = append(listOfCsvFiles, path)
		}
		return nil
	}

	filepath.Walk(root, visit)
	return listOfCsvFiles
}
