package analysis

import (
	"testing"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
)

var h = pricehistory.ParseCsvFileToPriceHistory("../../pricedata/^GSPC.csv")

// func TestCalculateStochastic(t *testing.T) {

// 	type args struct {
// 		priceHistory     pricehistory.HistoricPrices
// 		averagingPeriodD int
// 		averagingPeriodK int
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want *StochasticLines
// 	}{
// 		// TODO: Add test cases.
// 		{"no clue", args{h, 3, 14}, &StochasticLines{97.02, 97.34, 0, 0}},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := CalculateStochastic(tt.args.priceHistory, tt.args.averagingPeriodD, tt.args.averagingPeriodK); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("CalculateStochastic() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func BenchmarkCalculateStochastic(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CalculateStochastic(h, 3, 14)
	}
}

func BenchmarkHighLow(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FindHighLow(h)
	}
}
