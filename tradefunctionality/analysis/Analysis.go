package analysis

import (
	"fmt"
	"math"
	"bitbucket.org/ll01/tradebot/tradefunctionality/pricehistory"
)

/*CalculateRSI uses historic data to calculate the Relative Strength Index.
The period count is the amount of trades to use to "smooth" the average stick to 14
*/
func CalculateRSI(periodCount int, currentPriceHistory []pricehistory.HistoricPrice) float64 {
	// source for calculation =  http://www.iexplain.org/ema-how-to-calculate/

	var gainEma float64
	var lossEma float64
	periodCountint := int(periodCount)
	for i := 1; i <= periodCountint; i = i + 1 {
		var change = math.Abs(currentPriceHistory[i].Close - currentPriceHistory[i-1].Close)

		if currentPriceHistory[i].Close > currentPriceHistory[i-1].Close {

			gainEma += change
		} else {
			lossEma += change
		}
	}

	gainEma = gainEma / float64(periodCount)
	lossEma = lossEma / float64(periodCount)

	if len(currentPriceHistory) > int(periodCount) {
		for i := periodCountint + 1; i < len(currentPriceHistory); i++ {
			var change = math.Abs(currentPriceHistory[i].Close - currentPriceHistory[i-1].Close)

			if currentPriceHistory[i].Close > currentPriceHistory[i-1].Close {

				gainEma = CalculateExponentialMovingAverage(change, gainEma, periodCount)
				lossEma = CalculateExponentialMovingAverage(0, lossEma, periodCount)
			} else {
				lossEma = CalculateExponentialMovingAverage(change, lossEma, periodCount)
				gainEma = CalculateExponentialMovingAverage(0, gainEma, periodCount)
			}

		}
	}
	var relativeStrength = gainEma / lossEma
	return 100 - (100 / (1 + relativeStrength))
}

/*CalculateStochasticMomentumIndex calculats the stochastic rsi with the data given
where x and y are smothing periods. n period is how maney periods should be looked at*/
func CalculateStochasticMomentumIndex(priceHistory pricehistory.HistoricPrices, periodCountN, periodCountXandY int) float64 {
	lowestLow, highestHigh := FindHighLow(priceHistory)
	var midPoint = (highestHigh + lowestLow) / 2
	var offset = len(priceHistory) - periodCountN
	var arrayOfMidPoints []float64
	for index := 0; index < periodCountN; index++ {
		var midPointDelta = priceHistory[offset].Close - midPoint
		arrayOfMidPoints = append(arrayOfMidPoints, midPointDelta)
		offset++
	}
	var MidPointDataSmoothed = CalculateDoubleExponentialMovingAverage(arrayOfMidPoints, 3, 3)
	var One = CalculateExponentialMovingAverage((highestHigh - lowestLow), (highestHigh - lowestLow), 3)
	var Two = One
	Two = CalculateExponentialMovingAverage(Two, One, 3)
	var output = 100 * (MidPointDataSmoothed / (Two / 2))
	fmt.Println(output)
	return output
}

//CalculateStochastic trading formular that calculatest the slow and fast moving stotastic
func CalculateStochastic(priceHistory pricehistory.HistoricPrices,
	averagingPeriodD, averagingPeriodK int) *StochasticLines {
	var stochasticLines StochasticLines
	var listOfFastLines = make([]float64, averagingPeriodD, averagingPeriodD)
	var start = len(priceHistory) - (averagingPeriodK)
	var end = start + averagingPeriodK
	for i := 0; i < averagingPeriodD; i++ {

		var TradingPeriodData = priceHistory[start:end]
		lowestInPeriod, highestInPeriod := FindHighLow(TradingPeriodData)
		var closingPrice = TradingPeriodData[len(TradingPeriodData)-1]
		FastLine := 100 * ((closingPrice.Close - lowestInPeriod) / (highestInPeriod - lowestInPeriod))
		listOfFastLines[i] = FastLine
		start--

		end--
	}

	var SlowLine = CalculateMean(listOfFastLines)
	stochasticLines.FastLine = listOfFastLines[0]
	stochasticLines.SlowLine = SlowLine
	return &stochasticLines
}

//StochasticLines object to hold both the slow and fast lines of the stotatsic
type StochasticLines struct {
	FastLine  float64
	SlowLine  float64
	upIndex   int
	downIndex int
}

// FindHighLow finds the highest high and lowest low in a historic price dataset
func FindHighLow(priceHistory []pricehistory.HistoricPrice) (lowest, highest float64) {
	lowest = priceHistory[0].Low
	highest = priceHistory[0].High

	for i := 0; i < len(priceHistory); i++ {
		if lowest > priceHistory[i].Low {
			lowest = priceHistory[i].Low
		} else if highest < priceHistory[i].High {
			highest = priceHistory[i].High

		}
	}

	return lowest, highest
}

/*CalculateDoubleExponentialMovingAverage Finds the moving average of the moving average using the first and second
periods as the averaging periods respectively */
func CalculateDoubleExponentialMovingAverage(data []float64, firstPeriod, secondPeriod int) float64 {
	_, firstMovingAverageArray := CalculateExponentialMovingAverageFromArray(data, firstPeriod)
	_, secondMovingAverageArray := CalculateExponentialMovingAverageFromArray(firstMovingAverageArray, secondPeriod)
	return secondMovingAverageArray[len(secondMovingAverageArray)-1]
}

/*CalculateExponentialMovingAverage the exponential moving average rates
later/more relevant trades higher so it tracks trends faster in theory
this should be the default moving average calculator*/
func CalculateExponentialMovingAverage(currentPrice, previousEma float64, numberOfDays int) (ema float64) {
	var k = 2 / (float64(numberOfDays) + 1)
	return previousEma + k*(currentPrice-previousEma)
}

/*CalculateExponentialMovingAverageFromArray  calculates the moving average for a given array*/
func CalculateExponentialMovingAverageFromArray(data []float64, periods int) (float64, []float64) {
	var output = data[0]
	var arrayOutput []float64
	for index := 0; index < len(data); index++ {
		output = CalculateExponentialMovingAverage(data[index], output, periods)
		arrayOutput = append(arrayOutput, output)
	}
	return output, arrayOutput
}

/*CalculateSimpleMovingAverage Calculates the moving average of datasets it is simple but
in theory is inferior to the exponential moving average. So only use this when debugging*/
func CalculateSimpleMovingAverage(currentPrice, previousEma float64, numberOfDays int) (ema float64) {
	return ((previousEma * float64(numberOfDays-1)) + currentPrice) / float64(numberOfDays)

}

// CalculateBollingerBands Calculates the Bollinger Bands of a dataset given the smoothing period
func CalculateBollingerBands(priceHistory []float64, periodCount int) (upperBand, middleBand, lowerBand float64) {
	var Ema float64

	for i := int(periodCount) + 1; i < len(priceHistory); i++ {
		Ema = CalculateExponentialMovingAverage(priceHistory[i], Ema, periodCount)
	}
	var standardDeviation = CalculateStandardDeviation(priceHistory[len(priceHistory)-20 : len(priceHistory)-1])
	upperBand = Ema + (standardDeviation * 2)
	lowerBand = Ema - (standardDeviation * 2)
	return upperBand, Ema, lowerBand
}

//CalculateStandardDeviation  Calculates the standard deviation of a dataset
func CalculateStandardDeviation(priceHistory []float64) float64 {
	var varianceSum float64
	var mean = CalculateMean(priceHistory)
	for i := 0; i < len(priceHistory); i++ {
		var differenceSquared = math.Pow(mean-priceHistory[i], 2)
		varianceSum += differenceSquared
	}
	var variance = varianceSum / float64(len(priceHistory))
	return math.Sqrt(variance)
}

//CalculateMean Calculates the mea of a dataset
func CalculateMean(priceHistory []float64) float64 {
	var total float64
	for i := 0; i < len(priceHistory); i++ {
		total += priceHistory[i]
	}
	return total / float64(len(priceHistory))
}

//CalculatePriceVolidity calculated the price volidy for the intervals
func CalculatePriceVolidity(priceHistory pricehistory.HistoricPrices) float64 {
	var output []float64
	for i := 0; i < len(priceHistory); i++ {
		var volidityForPeriod = priceHistory[i].High / priceHistory[i].Low
		output = append(output, volidityForPeriod)
	}
	return CalculateMean(output)

}


func Min(data []float64) float64 {
	output := data[0]
	for _, item := range data {
		if item < output {
			output = item
		}
	}
	return output
}
func Max(data []float64) float64 {
	output := data[0]
	for _, item := range data {
		if item > output {
			output = item
		}
	}
	return output
}
