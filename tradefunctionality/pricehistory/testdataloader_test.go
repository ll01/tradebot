package pricehistory

import (
	"reflect"
	"testing"
)

func TestParseCsvFile(t *testing.T) {
	type args struct {
		filePath string
	}
	tests := []struct {
		name string
		args args
		want HistoricPrices
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseCsvFileToPriceHistory(tt.args.filePath); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseCsvFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLoadBinanceWebsocketJSON(t *testing.T) {
	type args struct {
		filePath string
	}
	tests := []struct {
		name string
		args args
		want [][]byte
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LoadBinanceWebsocketJSON(tt.args.filePath); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LoadBinanceWebsocketJSON() = %v, want %v", got, tt.want)
			}
		})
	}
}
