package pricehistory

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"strconv"
	"time"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
)

// HistoricPrice Data structure that stores open close and high low prices
type HistoricPrice struct {
	Open        float64
	Close       float64
	High        float64
	Low         float64
	TimeOfPrice time.Time
}

// NewHistoricPrice creates a HistoricPrice Object
func NewHistoricPrice(open, close, high, low float64, time time.Time) HistoricPrice {
	var newHistoricPrice HistoricPrice

	newHistoricPrice.Open = open
	newHistoricPrice.Close = close
	newHistoricPrice.High = high
	newHistoricPrice.Low = low
	// var timeOfTrade, err = time.Parse(time.RFC3339, timeAsString+"Z")
	// crash.PanicError(err)
	newHistoricPrice.TimeOfPrice = time
	return newHistoricPrice
}

// HistoricPrices is an array of a Historic price
type HistoricPrices []HistoricPrice

//GetPrices returns all the closing prices as a array of float64
func (prices HistoricPrices) GetPrices() []float64 {
	var listOfPrices []float64
	for _, HistoricPrice := range prices {
		listOfPrices = append(listOfPrices, HistoricPrice.Close)
	}
	return listOfPrices
}

//GetLastPrice returns latest price for market
func (prices HistoricPrices) GetLastPrice() HistoricPrice {
	return prices[len(prices)-1]
}

type getpricehistory func(marketString, tickInterval string) (HistoricPrices, error)

type priceHistoryJob struct {
	market string
	prices HistoricPrices
}

func GetHistorys(markets []string, interval string,
	histories getpricehistory) map[string]HistoricPrices {
	result := make(map[string]HistoricPrices)
	historyChan := make(chan priceHistoryJob, len(markets))
	for _, market := range markets {
		go HistoryJob(market, interval, histories, historyChan)
	}
	for range markets {
		select {
		case newHistory := <-historyChan:
			result[newHistory.market] = newHistory.prices
		}
	}

	return result
}

func HistoryJob(market, interval string, histories getpricehistory,
	result chan priceHistoryJob) {
	pricehistory, err := histories(market, interval)
	crash.PanicError(err)
	result <- priceHistoryJob{market, pricehistory}
}

//ParseCsvFile turns csv of candels to HistoricPricesObject
func ParseCsvFile(filePath string) HistoricPrices {
	var data HistoricPrices
	reader := openCSV(filePath)
	reader.Read()
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else {
			crash.PanicError(err)
		}
		open, err := strconv.ParseFloat(line[1], 64)
		crash.PanicError(err)
		close, err := strconv.ParseFloat(line[4], 64)
		crash.PanicError(err)
		high, err := strconv.ParseFloat(line[2], 64)
		crash.PanicError(err)
		low, err := strconv.ParseFloat(line[3], 64)
		crash.PanicError(err)

		timeOfTrade, err := time.Parse("2006-01-02", line[0])
		data = append(data, NewHistoricPrice(open, close, high, low, timeOfTrade))
	}
	return data
}
func openFile(filePath string) io.Reader {
	csvFile, err := os.Open(filePath)
	crash.PanicError(err)
	return bufio.NewReader(csvFile)
}

func openCSV(filePath string) *csv.Reader {
	bufferReader := openFile(filePath)
	return csv.NewReader(bufferReader)
}
