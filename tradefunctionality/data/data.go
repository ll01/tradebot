package data

import (
	"fmt"
	"time"

	"github.com/shopspring/decimal"
)

const (
	Buy       = "buy"
	Sell      = "sell"
	Filled    = "filled"
	Waiting   = "waiting"
	Cancelled = "cancelled"
)

type Trade struct {
	ID        string
	RawID     string
	Market    string
	Side      string
	Reference string
	Status    string
	Rate      decimal.Decimal
	Amount    decimal.Decimal
	Time      *time.Time
}

func (t *Trade) ToString() string {
	return fmt.Sprintf("id :%s, market :%s, rate :%s\n"+
		" amount %s, side: %s, status :%s, ratio :%s, time of trade :%s",
		t.ID, t.Market, t.Rate.String(), t.Amount.String(), t.Status,
		t.Time.String())
}

// func (t *Trade) GenerateExtract() TradeExtract {
// 	return TradeExtract{MarketID: t.Market, ID: t.RawID, Side: t.Side,
// 		Quantity: t.Amount.String()}
// }

//NewTrade creates new trade object
func NewTrade(localID, exchangeID, market, side, ref, status string, rate,
	quantity decimal.Decimal, time *time.Time) Trade {
	newTrade := Trade{ID: localID, RawID: exchangeID, Market: market,
		Rate: rate, Amount: quantity, Side: side,
		Status: status, Reference: ref, Time: time}
	return newTrade
}

// type TradeExtract struct {
// 	MarketID string
// 	ID       string
// 	Side     string
// 	Quantity string
// 	Filled   bool
// }
type Transaction struct {
	Rate     float64
	quantity float64
}

type HistoricTrade struct {
	ID string
}
