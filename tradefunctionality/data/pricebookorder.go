package data

type OrderBookType int

const (
	Ask OrderBookType = iota
	Bid
)

type Book struct {
	OrderMap map[float64]string
	Keys     []float64
}

type OrderBook struct {
	LastUpdateId int64
	Depth        int
	IsSim        bool
	AskOrderBook Book
	BidOrderBook Book
	PriceList    []float64
}

type OrderBookValues struct {
	Ask float64
	Bid float64
}

func (v *OrderBookValues) Get(orderType OrderBookType) float64 {
	var value = v.Bid
	if orderType == Ask {
		value = v.Ask
	}
	return value
}
