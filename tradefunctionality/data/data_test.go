package data

import (
	"os"
	"reflect"
	"sort"
	"testing"

	"github.com/buger/jsonparser"

	"github.com/shopspring/decimal"
)

func setupTradeMap(testBinaryFilePath string) TradeMap {
	var data TradeMap
	data = make(map[string]*Trade)
	ID := "1"
	market := "testmarket"
	decimalValue := decimal.NewFromFloat(1)
	side := "buy"
	newTrade := &Trade{ID: ID, Market: market,
		Rate: decimalValue, Amount: decimalValue, Side: side, Filled: false}
	data[ID] = newTrade
	return data
}

func TestSerialize(t *testing.T) {
	testBinaryFilePath := "./testBinary.bin"
	data := setupTradeMap(testBinaryFilePath)
	serialize(data, testBinaryFilePath)
	if _, err := os.Stat(testBinaryFilePath); os.IsNotExist(err) {
		t.Error("failed to create bin")
	}

}

func TestDeserialize(t *testing.T) {
	testBinaryFilePath := "./testBinary.bin"
	data := setupTradeMap(testBinaryFilePath)
	serialize(data, testBinaryFilePath)
	dataSaved := deserialize(testBinaryFilePath)
	areEqual := reflect.DeepEqual(data, dataSaved)
	if areEqual == false {
		t.Error("failed to deserialize")
	}
}

var floats1000elements = makeArrayOfFloats(1000)
var floats10elements = makeArrayOfFloats(10)
var floats20elements = makeArrayOfFloats(20)

func BenchmarkInOrder1000(b *testing.B) {

	for n := 0; n < b.N; n++ {
		_ = deleteInOrder(floats1000elements, 5)

	}
}

func BenchmarkOutOfOrder1000(b *testing.B) {

	for n := 0; n < b.N; n++ {
		_ = deleteOutOfOrder(floats10elements, 5)

	}
}

func BenchmarkOutOfOrder1000WithSort(b *testing.B) {
	for n := 0; n < b.N; n++ {
		newfloats := deleteOutOfOrder(floats1000elements, 5)
		sort.Float64s(newfloats)

	}
}

func BenchmarkInOrder10(b *testing.B) {

	for n := 0; n < b.N; n++ {
		_ = deleteInOrder(floats10elements, 5)

	}
}

func BenchmarkOutOfOrder10(b *testing.B) {

	for n := 0; n < b.N; n++ {
		_ = deleteOutOfOrder(floats10elements, 5)

	}
}

func BenchmarkOutOfOrder10WithSort(b *testing.B) {
	for n := 0; n < b.N; n++ {
		newfloats := deleteOutOfOrder(floats20elements, 5)
		sort.Float64s(newfloats)

	}
}

var rawPriceData = []byte(`{"lastUpdateId":30502115,"bids":[["0.00307900","9.74000000",[]],["0.00307500","9.75000000",[]],["0.00307300","3.26000000",[]],["0.00306900","9.76000000",[]],["0.00305800","529.73000000",[]]],"asks":[["0.00309400","382.14000000",[]],["0.00309900","141.82000000",[]],["0.00311000","3.94000000",[]],["0.00313600","867.95000000",[]],["0.00313900","210.79000000",[]]]}`)

func BenchmarkUnmarshalTime(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, _ = jsonparser.GetInt(rawPriceData, "lastUpdateId")

	}
}

type x struct {
	a decimal.Decimal
	b decimal.Decimal
}

func BenchmarkUnmarshalPriceObjects(b *testing.B) {
	for n := 0; n < b.N; n++ {
		var book []x
		jsonparser.ArrayEach(rawPriceData, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			aS, _ := jsonparser.GetUnsafeString(value, "[0]")
			a, _ := decimal.NewFromString(aS)

			bS, _ := jsonparser.GetUnsafeString(value, "[1]")
			b, _ := decimal.NewFromString(bS)
			book = append(book, x{a, b})
		}, "bids")

	}
}

var priceToConvert = "0.00306900"

func BenchmarkNewDecimal(b *testing.B) {

	for n := 0; n < b.N; n++ {
		_, _ = decimal.NewFromString(priceToConvert)
		// _, _ = strconv.ParseFloat(priceToConvert, 64)
		// fmt.Println(hi)
	}

}

func makeArrayOfFloats(amountOfNumbers int) []float64 {
	var output []float64
	for i := 0; i < amountOfNumbers; i++ {
		output = append(output, float64(i))
	}
	return output
}

// func BenchmarkBytesContains(b *testing.B) {
// 	for n := 0; n < b.N; n++ {
// 		if bytes.Contains(data, []byte("executionReport")) {
// 		}
// 	}
// }
