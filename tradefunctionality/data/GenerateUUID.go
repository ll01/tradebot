package data

import (
	uuid "github.com/satori/go.uuid"
)

func GenerateUUID() string {
	var err error
	return uuid.Must(uuid.NewV4(), err).String()
}
