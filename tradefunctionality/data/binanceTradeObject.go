package data

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/dchest/uniuri"
)

type binanceTradeObject struct {
	e  string      `json:"e"`
	E  int64       `json:"E"`
	S1 string      `json:"s"`
	c  string      `json:"c"`
	S  string      `json:"S"`
	o  string      `json:"o"`
	f  string      `json:"f"`
	Q  string      `json:"q"`
	p  string      `json:"p"`
	P  string      `json:"P"`
	F  string      `json:"F"`
	G  int         `json:"g"`
	C  string      `json:"C"`
	x  string      `json:"x"`
	X  string      `json:"X"`
	r  string      `json:"r"`
	I1 int         `json:"i"`
	l  string      `json:"l"`
	z  string      `json:"z"`
	L  string      `json:"L"`
	n  string      `json:"n"`
	N  interface{} `json:"N"`
	T  int64       `json:"T"`
	t  int         `json:"t"`
	I  int         `json:"I"`
	w  bool        `json:"w"`
	m  bool        `json:"m"`
	M  bool        `json:"M"`
	O  int64       `json:"O"`
	Z  string      `json:"Z"`
	Y  string      `json:"Y"`
}

// func GenerateNew(tradeInfo *TradeInfo) *binanceTradeObject {
// 	var binanceExEReport binanceTradeObject

// 	binanceExEReport.e = "executionReport"
// 	binanceExEReport.E = time.Now().UnixNano()
// 	binanceExEReport.s = tradeInfo.Market
// 	binanceExEReport.c = uniuri.New()
// 	binanceExEReport.S = strings.ToUpper(tradeInfo.Side)
// 	binanceExEReport.o = "LIMIT"
// 	binanceExEReport.f = "GTC"
// 	binanceExEReport.q = tradeInfo.Quantity
// 	if binanceExEReport.o == "LIMIT" {
// 		binanceExEReport.P = "0.00000000"
// 	}
// 	binanceExEReport.x = "NEW"
// 	binanceExEReport.X = "NEW"
// 	binanceExEReport.r = "NONE"
// 	binanceExEReport.i = rnd.Int()
// 	binanceExEReport.l = "0.00000000"
// 	binanceExEReport.z = "0.00000000"
// 	binanceExEReport.L = "0.00000000"
// 	binanceExEReport.n = "0"
// 	binanceExEReport.N = nil
// 	binanceExEReport.T = tradeInfo.Time
// 	binanceExEReport.t = -1
// 	binanceExEReport.I = 0
// 	binanceExEReport.w = true
// 	if tradeInfo.Side == "buy" {
// 		binanceExEReport.m = false
// 	} else {
// 		binanceExEReport.m = true
// 	}
// 	binanceExEReport.M = false
// 	binanceExEReport.O = time.Now().UnixNano()

// 	binanceExEReport.Z = "0.00000000"
// 	binanceExEReport.Y = "0.00000000"
// 	return &binanceExEReport
// }
func GeneratePartal() {

}

func MakeTrade(data []byte) {
	// var tmp binanceTradeObject
	// err := json.Unmarshal(data, tmp)
	// crash.PanicError(err)
	// rate, _ := decimal.NewFromString(tmp.P)
	// quantity, _ := decimal.NewFromString(tmp.Q)
	// creationTime := time.Unix(0, tmp.O/1000)
	// CreateTrade(strconv.Itoa(tmp.I1), tmp.S1, strings.ToLower(tmp.S), "", rate,
	// 	quantity, &creationTime)

}

// var lock = sync.Mutex{}

func GenerateFilled(currentTime time.Time, trade *Trade) string {
	// lock.Lock()
	// defer lock.Unlock()
	var binanceExEReport binanceTradeObject

	binanceExEReport.e = "executionReport"
	binanceExEReport.E = currentTime.UnixNano()
	binanceExEReport.S1 = trade.Market
	binanceExEReport.c = uniuri.New()
	binanceExEReport.S = strings.ToUpper(trade.Side)
	binanceExEReport.o = "LIMIT"
	binanceExEReport.f = "GTC"
	binanceExEReport.Q = trade.Amount.StringFixedBank(8)
	if binanceExEReport.o == "LIMIT" {
		binanceExEReport.P = "0.00000000"
	}
	binanceExEReport.x = "FILLED"
	binanceExEReport.X = "FILLED"
	binanceExEReport.r = "NONE"
	binanceExEReport.I1, _ = strconv.Atoi(trade.RawID)
	binanceExEReport.l = trade.Amount.StringFixedBank(8)
	binanceExEReport.z = trade.Amount.StringFixedBank(8)
	binanceExEReport.L = trade.Amount.StringFixedBank(8)
	binanceExEReport.n = "0"
	binanceExEReport.N = "BNB"
	binanceExEReport.T = currentTime.UnixNano()
	binanceExEReport.t = -1
	binanceExEReport.I = 0
	binanceExEReport.w = true
	if trade.Side == "buy" {
		binanceExEReport.m = false
	} else {
		binanceExEReport.m = true
	}
	binanceExEReport.M = false
	binanceExEReport.O = trade.Time.UnixNano()

	binanceExEReport.Z = "0.00000000"
	binanceExEReport.Y = trade.Amount.Mul(trade.Rate).StringFixedBank(8)
	json, err := json.Marshal(binanceExEReport)
	if err != nil {
		log.Fatalf("not able to marshal struct %v\n", err)
	}
	return string(json)
}
