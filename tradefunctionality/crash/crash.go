package crash

import (
	"fmt"
	"log"
	"runtime/debug"
	"strings"
	"sync/atomic"
	"time"

	"github.com/line/line-bot-sdk-go/linebot"
	gomail "gopkg.in/gomail.v2"
)

const (
	userid = ""
)

var apiCall uint64

type saveFunction func()

var save saveFunction
var bot *linebot.Client

//Init makes Line bot
func Init(secret, token string) {
	var err error
	bot, err = linebot.New(secret, token)
	if err != nil {
		log.Fatalf("unable to make line bot error : %v\n", err.Error())
	}
}

// SetSaveOnCrash sets the function call that saves the program state
func SetSaveOnCrash(newSave saveFunction) {
	save = newSave
}

//PanicError if there is an error panic it and send a email to me
func PanicError(err error) {
	if err != nil {
		fmt.Printf("error occured at %v\n", time.Now())
		fmt.Println(err.Error())
		debug.PrintStack()
		if save != nil {
			save()
		}
		if isForceClose(err) || hasTimedOut(err) {
			// tradeClient = NewClient()
			fmt.Println(err.Error())
		} else {
			fmt.Println(apiCall)
			emailUser(err.Error())
			log.Fatal(err)

		}
	}
}

func isForceClose(err error) bool {
	return strings.Contains(err.Error(),
		"An existing connection was forcibly closed by the remote host.")

}

func sendPushNotification(message string) {
	lineMessage := linebot.NewTextMessage(message)
	bot.PushMessage(userid, lineMessage)
}

//emailUser sends the panic error to a email
func emailUser(messageToSend string) {
	m := gomail.NewMessage()
	m.SetHeader("From", "enti2@hotmail.co.uk")
	m.SetHeader("To", "enti2@hotmail.co.uk")
	m.SetHeader("Subject", "message from trading app")
	m.SetBody("text/html", messageToSend)
	//25
	d := gomail.NewDialer("smtp.live.com", 587,
		"enti2@hotmail.co.uk", "DragonQuest13!")
	if err := d.DialAndSend(m); err != nil {
		log.Fatal(err)
	}

}

func hasTimedOut(err error) bool {

	return strings.Contains(err.Error(),
		"connected party did not properly respond after a period of time")
}

//ApiCallAdd increments api calls
func ApiCallAdd() {
	atomic.AddUint64(&apiCall, 1)
}

//GetAPICall returns api call
func GetAPICall() uint64 {
	return atomic.LoadUint64(&apiCall)
}
