package logs

import (
	"database/sql"
	"fmt"
	"math"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/data"

	//provider for sqlite
	_ "github.com/mattn/go-sqlite3"
	"github.com/shopspring/decimal"
)

const (
	tableName        = "trades"
	dropTableQuery   = `drop table if exists '` + tableName + `'`
	exchangeIDColumn = "exchangeID"
	marketColumn     = "market"
	sideColumn       = "side"
	referenceColumn  = "reference"
	rateColumn       = "rate"
	amountColumn     = "amount"
	statusColumn     = "status"
	timeColumn       = "time"
	sqliteFormat     = "2006-01-02 15:04:05"
)

var createTableQuery = fmt.Sprintf(`
Create Table if not EXISTS '`+tableName+`' (
	id integer primary key autoincrement,
	%[1]s string ,
	%[2]s text not null,
	%[3]s text,
	%[4]s text, 
	%[5]s Text,
	%[6]s Text,
	%[7]s Text,
	%[8]s Date
	)`, exchangeIDColumn, marketColumn, sideColumn, referenceColumn, rateColumn, amountColumn,
	statusColumn, timeColumn)

var currentLog *Log
var isFresh = false
var databaseName = "test"
var debugFile = "debugLogs.txt"
var BinarydebugFile = "logs.bin"

//LogOnce the sync object to make a singleton log
var LogOnce sync.Once

//GetLog Gets the current logging instance
func GetLog() *Log {
	LogOnce.Do(func() {
		currentLog = NewLog()
	})
	return currentLog
}

//Log  datastructure for comunicating with log database
type Log struct {
	database *sql.DB
	fullLog  string
	total    float64
}

//Initalize sets up the logs for the program
func Initalize(freshStatus bool, newDatabaseName string) {
	isFresh = freshStatus
	databaseName = newDatabaseName
	debugFile = newDatabaseName + "_text_logs.txt"
	BinarydebugFile = newDatabaseName + "logs.bin"
}

//NewLog Constructor for log opens database and creates table
func NewLog() *Log {
	var log Log
	database, err := sql.Open("sqlite3", databaseName+".sqlite")
	crash.PanicError(err)
	log.database = database
	if isFresh {
		log.DropTable()
	}

	_, err = log.database.Exec(createTableQuery)
	crash.PanicError(err)

	return &log
}

// //Close Finalizer for Log struct
// func (log *Log) Close() {
// 	log.database.Close()
// }

//DropTable drops the table working on
func (log *Log) DropTable() {
	_, err := log.database.Exec(dropTableQuery)
	crash.PanicError(err)
}

// func (log *Log) SelectAll() {
// 	stmt := "select * from " + tableName
// 	log.makeQuery(stmt)
// }

func buildInsertStatement(tableName string)  string{
	return fmt.Sprintf(`insert into "%[1]s" ("%[2]s", "%[3]s", "%[4]s", "%[5]s",
	 "%[6]s", "%[7]s", "%[8]s", "%[9]s") values(?,?,?,?,?,?,?,?)`,tableName,
	exchangeIDColumn, marketColumn, sideColumn, referenceColumn,
	rateColumn, amountColumn, statusColumn, timeColumn,)
}

//InsertTradeIntoTable inserts info into database
func (log *Log) InsertTradeIntoTable(trade *data.Trade) {
	stmt, err := log.database.Prepare(buildInsertStatement(tableName))
	crash.PanicError(err)
	defer stmt.Close()
	//TODO perfom transaction on insert
	result, err := stmt.Exec(
		 trade.RawID, trade.Market,
		trade.Side, trade.Reference, trade.Rate.String(), trade.Amount.String(),
		trade.Status, trade.Time)
	crash.PanicError(err)
	newID, err := result.LastInsertId()
	crash.PanicError(err)
	trade.ID = strconv.FormatInt(newID, 10)
	// fmt.Println(tradeType)
}

// UpdateStatus updates the status of a trade given its local uuid
func (log *Log) UpdateStatus(id, status string) {
	stmt, err := log.database.Prepare("update " + tableName +
		" set status = ?  where id = ?")
	crash.PanicError(err)
	defer stmt.Close()
	_, err = stmt.Exec(status, id)
	crash.PanicError(err)
}

//CalculateSalesCounter calculates the exponential groth counter for sales
func (log *Log) CalculateSalesCounter(profitRatio float64) *sync.Map {
	var output sync.Map
	stmt, err := log.database.Prepare("select marketString, min( amount) as first," +
		"max(amount) as last from " + tableName +
		` where type = "sell" group by(marketString);`)
	crash.PanicError(err)
	result, err := stmt.Query()
	for result.Next() {
		var marketID string
		var first float64
		var last float64
		err = result.Scan(&marketID, &first, &last)
		crash.PanicError(err)
		trades := math.Log(last/first) / math.Log(profitRatio)
		tradesInt := int(math.Floor(trades))
		output.Store(marketID, tradesInt)
	}
	crash.PanicError(err)

	return &output
}

func (log *Log) makeQuery(sql string, args ...interface{}) *sql.Rows {
	stmt, err := log.database.Prepare(sql)
	crash.PanicError(err)
	defer stmt.Close()
	rows, err := stmt.Query(args)
	crash.PanicError(err)
	return rows
}

//TradeData export struct for trading data
type TradeData struct {
	UUID   string
	Amount decimal.Decimal
	Rate   decimal.Decimal
}

//GetTrades gets trade info from database
func (log *Log) GetTrades(marketString string, status string) []TradeData {
	var sqlStatmet = "select  (uuid, rate, amount) from " + tableName +
		" where status = ? "
	if marketString != "" {
		sqlStatmet += " and marketString = ?"
	}
	var trades []TradeData
	stmt, err := log.database.Prepare(sqlStatmet)
	crash.PanicError(err)
	defer stmt.Close()
	rows, err := stmt.Query(status, marketString)
	crash.PanicError(err)
	for rows.Next() {
		tradeData := TradeData{}
		var amountString string
		var rateString string

		err = rows.Scan(&tradeData.UUID, &amountString, &rateString)
		tradeData.Amount, err = decimal.NewFromString(amountString)
		crash.PanicError(err)
		tradeData.Rate, err = decimal.NewFromString(rateString)
		crash.PanicError(err)
		trades = append(trades, tradeData)
	}

	return trades
}

//SaleCount struct holding name of market and amout of sales got
type SaleCount struct {
	MarketID string
	Count    int
}

//CountSalesMade gets the amount of sales made in the database
func (log *Log) CountSalesMade() []SaleCount {
	var output []SaleCount
	stmt, err := log.database.Prepare("select marketString, count(*) from " +
		tableName + " where type = 'sell' and status = 'filled' group by (marketString)")
	crash.PanicError(err)
	rows, err := stmt.Query()
	crash.PanicError(err)
	defer rows.Close()
	var count = 0
	var marketID = ""
	for rows.Next() {
		rows.Scan(&marketID, &count)
		output = append(output, SaleCount{marketID, count})
	}
	return output
}

//CalculateRevinue caulates the total revinue for a trade's
// buy or selll
func (log *Log) CalculateRevinue(typeString string) float64 {
	stmt, err := log.database.Prepare("select rate, amount from " +
		tableName + " where type = '" + typeString + "'")
	crash.PanicError(err)

	rows, err := stmt.Query()
	crash.PanicError(err)
	var rate float64
	var amount float64
	var revenue float64
	for rows.Next() {

		rows.Scan(&rate, &amount)
		var total = (rate * amount)
		revenue += total

	}
	return revenue
}

//CalculateCommision calculates the exchanges cut of the revinue
func (log *Log) CalculateCommision(commitionRate float64) float64 {
	stmt, err := log.database.Prepare("select rate, amount from " +
		tableName)
	crash.PanicError(err)
	rows, err := stmt.Query()
	crash.PanicError(err)
	var rate float64
	var amount float64
	var totalCommition float64
	for rows.Next() {

		rows.Scan(&rate, &amount)
		var commition = (rate * amount) * commitionRate
		totalCommition += commition
	}
	return totalCommition
}

func (log *Log) calculateProfit(marketString string) float64 {
	panic("not implemented ")
}

//GetUnsoldTrades gets trades with no link
func (log *Log) GetUnsoldTrades() []data.Trade {

	stmt, err := log.database.Prepare(fmt.Sprintf(
		`select * from %[1]s x where x.id NOT in(
		select x.id
		from %[1]s x, %[1]s y
		where x.exchangeID = y.reference) and x.side = ? and
		x.status != ?;`, tableName))
	crash.PanicError(err)
	rows, err := stmt.Query(data.Buy, data.Cancelled)
	var trades []data.Trade
	var localID, exchangeID, market, side, ref, rate, amount, status string
	var time time.Time

	for rows.Next() {
		// temp := data.Trade{}
		rows.Scan(&localID, &exchangeID, &market, &side, &ref, &rate, &amount,&status,&time)
		rateDecimal, err := decimal.NewFromString(rate)
		crash.PanicError(err)
		amountDecimal, err := decimal.NewFromString(amount)
		newTrade := data.NewTrade(localID, exchangeID, market, side, ref, status,
			rateDecimal, amountDecimal, &time)
		trades = append(trades, newTrade)
		fmt.Println(newTrade.ToString())
	}
	return trades
}

// //GetUnFilledSales
// func (log *Log) GetUnFilledSales() []data.Trade {
// 	stmt, err := log.database.Prepare(
// 		fmt.Sprintf(`select * from %[1]s where type = 'sell' and status = 'waiting'`, tableName))
// 	crash.PanicError(err)
// 	rows, err := stmt.Query()
// 	var uuidList []string
// 	uuid := ""
// 	for rows.Next() {
// 		rows.Scan(&uuid)
// 		uuidList = append(uuidList, uuid)
// 	}
// 	return uuidList
// }
