package logs

import (
	"testing"
)

func TestDecodeMessagesMessage(t *testing.T) {
	type args struct {
		filePath string
	}
	testArgs := args{filePath: "./testfile.bin"}
	words := []string{"my", "name", "is"}
	wants := []ReplayMessage{}
	for _, word := range words {
		LogMessage(word, testArgs.filePath)
		wants = append(wants, ReplayMessage{Message: word, ID: 0})
	}

	tests := []struct {
		name string
		args args
		want []ReplayMessage
	}{
		// TODO: Add test cases.
		{name: "single single entry", args: testArgs, want: wants[0:1]},
		{name: "single multiple entry", args: testArgs, want: wants},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DecodeMessages(tt.args.filePath); CheckAllMessages(got, tt.want) {
				// fmt.Println(got[0].Message == tt.want[0].Message)
				t.Errorf("DecodeMessages() = %v, want %v", got[0].Message, tt.want[0].Message)
			}
		})
	}
}

func CheckAllMessages(got, want []ReplayMessage) bool {
	result := true
	if len(got) != len(want) {
		result = false
	} else {
		for i := range got {
			if got[i].Message != want[i].Message {
				result = false
				break
			}
		}
	}
	return result

}
