package logs

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
)

// ReplayMessage used to send to a chanal to simulate live trading
type ReplayMessage struct {
	Message         string
	ID              int64
}

// LogMessage appends the message tp debugging  files
func LogMessage(msg, binarydebugFile string) {

	newmsg := ReplayMessage{msg, time.Now().Unix()}
	var buffer bytes.Buffer
	encoder := gob.NewEncoder(&buffer)
	err := encoder.Encode(newmsg)
	if err != nil {
		fmt.Printf("error encoding struct %v, error: %v \n ", newmsg, err)
	} else {
		appendBytes(binarydebugFile, buffer.Bytes())
	}

	msgToSave := msg + ",n/a," + strconv.FormatInt(time.Now().Unix(), 10)
	appendFile(debugFile, msgToSave)
}

func appendFile(fileName, text string) {

	file := openOrCreateFile(fileName)
	defer file.Close()

	_, err := file.WriteString(text + "\n")
	checkFileWrite(err)
}

func appendBytes(fileName string, data []byte) {
	file := openOrCreateFile(fileName)
	defer file.Close()
	_, err := file.Write(data)
	checkFileWrite(err)

}

func checkFileWrite(err error) {
	if err != nil {
		log.Fatalf("failed writing to file: %s", err)
	}
}

func openOrCreateFile(fileName string) *os.File {
	var fileToOpen *os.File

	if _, err := os.Stat(fileName); err == nil {
		fileToOpen, err = os.OpenFile(fileName, os.O_WRONLY|os.O_APPEND, 0644)
		crash.PanicError(err)
	} else if os.IsNotExist(err) {
		fileToOpen, err = os.Create(fileName)
		crash.PanicError(err)
	} else {
		log.Fatal(err.Error())
	}
	return fileToOpen
}

//DecodeMessages decodes binary file into replay messages
func DecodeMessages(filePath string) []ReplayMessage {
	text, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatalf("failed to read file %v error is, %v\n", filePath, err)
	}
	byteLines := bytes.Split(text, []byte("\n"))
	var messages []ReplayMessage
	for _, line := range byteLines {
		var buffer bytes.Buffer
		buffer.Write(line)
		decoder := gob.NewDecoder(&buffer)
		var message ReplayMessage

		decoder.Decode(&message)
		if message.Message != "" {
			fmt.Println(string(line))
		}
		//if message.mez
		messages = append(messages, message)
	}
	return messages
}

func openFile(filePath string) string {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatal(err)
	}
	return string(data)

}
