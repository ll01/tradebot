package spacer

import (
	"fmt"
	"sync/atomic"
	"time"
)

// Spacer object that spaces out the buys
// to better stop false positive buys
type Spacer struct {
	AmountOfAlloudTrades uint32
	tradeCounter         uint32
	timeToWaitInSeconds  int
	lastTrades           map[string]time.Time
}

// Initalize defines the amount of trades the program has and how long
// to wait for before makeing another trade
func Initalize(amountOfTrades, timeToWaitInSeconds, tradesFromLastSession int) *Spacer {
	var spacer Spacer
	spacer.AmountOfAlloudTrades = uint32(amountOfTrades)
	spacer.timeToWaitInSeconds = timeToWaitInSeconds
	spacer.tradeCounter = uint32(tradesFromLastSession)
	spacer.lastTrades = make(map[string]time.Time)
	return &spacer
}

// TrackMarket adds a market for the spacer to track
func (spacer *Spacer) TrackMarket(market string) {
	spacer.lastTrades[market] = time.Unix(0, 0).UTC()
}

//IsTradeok asks if the defind time to wait for has passed before buying again
func (spacer *Spacer) IsTradeok(timeOfLastTrade time.Time, currentExtchangeTime time.Time) bool {
	// var timeOfLastTrade = spacer.getLastTime(marketString)
	var isAvalible = false
	counter := atomic.LoadUint32(&spacer.tradeCounter)
	var beforeTime = timeOfLastTrade.Add(time.Duration(spacer.timeToWaitInSeconds) * time.Second)
	// fmt.Printf("currentTime : %v, before time %v\n", currentExtchangeTime.String(), beforeTime.String())
	if currentExtchangeTime.After(beforeTime) &&
		counter < spacer.AmountOfAlloudTrades {
		isAvalible = true
		// fmt.Printf("trade counter %v/n", spacer.tradeCounter)
		// fmt.Printf("max %v/n", spacer.AmountOfAlloudTrades)

	}
	return isAvalible
}

var hi uint

func (spacer *Spacer) getLastTime(market string) time.Time {
	if _, ok := spacer.lastTrades[market]; !ok {
		spacer.lastTrades[market] = time.Unix(0, 0).UTC()
	}
	return spacer.lastTrades[market]
}

func (spacer *Spacer) Increment(market string, currentTime time.Time) {
	spacer.lastTrades[market] = currentTime
	atomic.AddUint32(&spacer.tradeCounter, 1)

	// fmt.Println("counter incrementing : ", newCounter)
}

func (spacer *Spacer) Deccrement() {
	atomic.AddUint32(&spacer.tradeCounter, ^uint32(0))
	// fmt.Println("counter decrementing : ", newCounter)
}

func (spacer *Spacer) Copy() *Spacer {
	var s Spacer
	s.AmountOfAlloudTrades = spacer.AmountOfAlloudTrades
	s.lastTrades = make(map[string]time.Time)
	s.timeToWaitInSeconds = spacer.timeToWaitInSeconds
	s.tradeCounter = 0
	return &s
}
