package spacer

import (
	"testing"
	"time"
)

func TestSpacer_IsTradeok(t *testing.T) {
	currentTime := time.Now()
	var waitTime uint = 10
	beforeWaitTime := currentTime.Add(-time.Duration(waitTime-1) * time.Second)
	afterWaitTime := currentTime.Add(-time.Duration(1+waitTime) * time.Second)
	type fields struct {
		AmountOfAlloudTrades uint32
		tradeCounter         uint32
		timeToWaitInSeconds  uint
		lastTrades           map[string]time.Time
	}
	type args struct {
		marketString         string
		currentExtchangeTime time.Time
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		// TODO: Add test cases.
		{"under counter after time lock", fields{2, 1, waitTime, map[string]time.Time{"market1": afterWaitTime}}, args{"market1", currentTime}, true},
		{"under counter before time lock", fields{2, 1, waitTime, map[string]time.Time{"market1": beforeWaitTime}}, args{"market1", currentTime}, false},
		{"over counter after time lock", fields{2, 2, waitTime, map[string]time.Time{"market1": afterWaitTime}}, args{"market1", currentTime}, false},
		{"over counter over time lock", fields{2, 2, waitTime, map[string]time.Time{"market1": beforeWaitTime}}, args{"market1", currentTime}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			spacer := &Spacer{
				AmountOfAlloudTrades: tt.fields.AmountOfAlloudTrades,
				tradeCounter:         tt.fields.tradeCounter,
				timeToWaitInSeconds:  tt.fields.timeToWaitInSeconds,
				lastTrades:           tt.fields.lastTrades,
			}
			if got := spacer.IsTradeok(tt.args.marketString, tt.args.currentExtchangeTime); got != tt.want {
				t.Errorf("Spacer.IsTradeok() = %v, want %v test %v", got, tt.want, tt.name)
			}
		})
	}
}
