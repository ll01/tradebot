package manageresources

import (
	"log"
	"os"
	"path/filepath"
)

func GetResourceFilePath(resourceRelativePath string) string {
	ex, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	exPath := filepath.Dir(ex)
	return filepath.Join(exPath, resourceRelativePath)
}
