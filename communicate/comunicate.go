package communicate

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/crash"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"

	"nanomsg.org/go/mangos/v2"
	"nanomsg.org/go/mangos/v2/protocol/respondent"

	// register transports
	_ "nanomsg.org/go/mangos/v2/transport/all"
)

const (
	serverName         = "BOTSERVER"
	survayurl          = "wss://survey.secondwind.app"
	nameurl            = "http://bot.secondwind.app/sub"
	dateReponse        = "DATE"
	pingReponse        = "PING"
	textLogsResponse   = "LOGTEXT"
	binaryLogsResponse = "LOGBIN"
	tradeResponse      = "TRADES"
	databaseResponse   = "DB"
)

var clientName = "N/A"

//Respondent responds to survay from server
func Respondent() {
	var sock mangos.Socket
	var err error
	var msg []byte

	sock, err = respondent.NewSocket()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	err = sock.Dial(survayurl)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	clientName = getName()
	for {
		msg, err = sock.Recv()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		if globaltime.GetGlobalTime().GetIsDebugging() == 3 {
			fmt.Printf("CLIENT(%s): RECEIVED \"%s\" SURVEY REQUEST\n",
				clientName, string(msg))
		}

		handelRequests(sock, msg)
	}
}

func getName() string {
	req, err := http.NewRequest("GET", nameurl, nil)
	crash.PanicError(err)
	req.Header.Set("message", "Hello")
	client := &http.Client{Timeout: time.Second * 10}
	resp, err := client.Do(req)
	crash.PanicError(err)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	crash.PanicError(err)
	return string(body)
}

func handelRequests(sock mangos.Socket, msgBytes []byte) {
	msg := string(msgBytes)
	switch msg {
	case dateReponse:
		d := date()
		sendMessage(sock, dateReponse, d)
	case pingReponse:
		sendMessage(sock, pingReponse, "ping")
	case textLogsResponse:
		logData, err := ioutil.ReadFile("debugLogs.txt")
		if err == nil {
			lines := strings.Split(string(logData), "\n")
			if len(lines) > 50 {
				lines = lines[50:]
			}
			dataToSend := strings.Join(lines, "\n")
			sendMessage(sock, textLogsResponse, dataToSend)
		} else {
			fmt.Printf("UNABLE TO Read debugLogs.txt error:%v\n", err.Error())
		}
	case binaryLogsResponse:
		logData, err := ioutil.ReadFile("logs.bin")
		if err == nil {
			sendBytes(sock, binaryLogsResponse, logData)
		} else {
			fmt.Printf("UNABLE TO Read Logs.bin error:%v\n", err.Error())
		}
	case tradeResponse:
		currentTrades := controller.GetAllTrades()
		data, err := json.Marshal(currentTrades)
		if err == nil {
			sendBytes(sock, tradeResponse, data)
		} else {
			fmt.Printf("UNABLE TO Read current trades error:%v\n", err.Error())
		}
	case databaseResponse:

	}
}

// response standerd response format for Request
type response struct {
	Name         string
	ResponseName string
	Data         []byte
}

func createResponse(responseName string, msg []byte) []byte {
	var tmp response
	tmp.Name = clientName
	tmp.ResponseName = responseName
	tmp.Data = msg
	data, err := json.Marshal(tmp)
	crash.PanicError(err)
	return data
}

func sendMessage(sock mangos.Socket, responseName, msg string) {
	sendBytes(sock, responseName, []byte(msg))
}

func sendBytes(sock mangos.Socket, responseName string, msgBytes []byte) {
	dataToSend := createResponse(responseName, msgBytes)
	err := sock.Send(dataToSend)
	if err != nil {
		crash.PanicError(err)
	} else {
		fmt.Printf("%s: SENT RESPONCE TO %s SUCCESSFULLY\n", serverName, msgBytes)
	}
}

func date() string {
	return time.Now().Format(time.ANSIC)
}
