package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"time"

	"bitbucket.org/ll01/tradebot/communicate"
	"bitbucket.org/ll01/tradebot/manageresources"
	"bitbucket.org/ll01/tradebot/tradefunctionality/controller"
	"bitbucket.org/ll01/tradebot/tradefunctionality/globaltime"
	"bitbucket.org/ll01/tradebot/tradefunctionality/logs"
	"bitbucket.org/ll01/tradebot/tradefunctionality/settings"
	"bitbucket.org/ll01/tradebot/tradefunctionality/trade"
)

/* general settings for trading program should be set hear
(in the future some will be r) replaced with flags*/
const (
	settingsFile = "settings.json"
)

var finishedTrading = false
var validIntervalNumbers map[int]string
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
var memprofile = flag.String("memprofile", "", "write memory profile to `file`")
var debug = flag.Bool("debugflag", false, "setts the program to run in debug mode")

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()
	addCPUProfiling()

	settingsFileLocation := manageresources.GetResourceFilePath(settingsFile)
	var setting *settings.Settings
	if len(os.Args) == 2 {
		setting = settings.Setup(os.Args[1])
	} else {
		setting = settings.Setup(settingsFileLocation)
	}
	// setting := settings.Setup(`F:\Programming\Go\src\tradebot\settings.json`)
	// setting = settings.Setup(`/Users/mcphersd/Go/src/tradebot/settings.json`)
	// crash.Init(setting.LineSecret, setting.LineToken)
	globaltime.Init(setting)
	if setting.IsLive == true || !setting.IsSimming() {
		go communicate.Respondent()
	}

	logs.Initalize(setting.IsFresh, setting.Databasename)
	controller.Connect(setting.IsFresh)

	clusterList := trade.NewClusterList(setting)

	trade.Run(clusterList, "interval", setting.IsLive)
	addMemoryProfiling()
}

func addMemoryProfiling() {
	if *memprofile != "" {
		f, err := os.Create(CreateProfileString(*memprofile))
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
		f.Close()
	}
}

func addCPUProfiling() {
	if *cpuprofile != "" {
		f, err := os.Create(CreateProfileString(*cpuprofile))
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
}

// CreateProfileString points where to put the profile strings
func CreateProfileString(profileName string) string {
	profilePath := "./profiles/" + time.Now().String() + profileName
	fmt.Println(profilePath)
	return profilePath
}
